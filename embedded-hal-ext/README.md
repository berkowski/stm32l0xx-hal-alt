# emedded-hal-ext: Additional traits for embedded targets

A few extra traits for embedded targets that I have found useful for projects.
Adds some capabilities I wanted that were missing from [`embedded-hal`](https://github.com/rust-embedded/embedded-hal) while that crate was in the beginning of it's major `v1.0` rewrite.
