#![no_std]

extern crate nb;

/// Enable a peripheral
pub trait EnablePeripheral {
    type Error;

    fn enable(&mut self) -> Result<(), Self::Error>;
    fn is_enabled(&self) -> Result<bool, Self::Error>;
}

/// Disable a peripheral
pub trait DisablePeripheral: EnablePeripheral {
    fn disable(&mut self) -> Result<(), Self::Error>;
}

pub struct TimeoutExpired {}

pub struct Timeout<'a, T> {
    timeout: u32,
    poll_interval: u32,
    delay: &'a mut T,
}

impl<'a, T> Timeout<'a, T>
where
    T: embedded_hal::blocking::delay::DelayMs<u32>,
{
    pub fn new(timeout: u32, poll_interval: u32, delay: &'a mut T) -> Self {
        Self {
            timeout,
            poll_interval,
            delay,
        }
    }

    pub fn reset(&mut self, timeout: u32) {
        self.timeout = timeout;
    }

    pub fn remaining(&self) -> u32 {
        self.timeout
    }

    pub fn spin(&mut self) -> Result<(), TimeoutExpired> {
        if self.timeout == 0 {
            return Err(TimeoutExpired {});
        }

        self.timeout = self.timeout.saturating_sub(self.poll_interval);
        self.delay.delay_ms(self.poll_interval);
        Ok(())
    }

    pub fn free(self) -> &'a mut T {
        self.delay
    }
}

pub mod serial {
    pub mod nb {

        /// Error writing to DMA buffer
        ///
        /// Most typical case of this error is the DMA buffer is too small to hold the desired
        /// data.
        #[derive(defmt::Format, Debug)]
        pub enum Error {
            BufferToSmall,
            PeripheralDisabled,
            Other,
        }

        pub trait WriteMany<T> {
            /// Write data into the provided byte buffer
            ///
            /// This method is intended to provide access to the underlying DMA buffer
            /// for the serial port, thus avoiding formatting a secondary buffer only to copy
            /// into the DMA buffer.
            ///
            /// This method MUST ensure that no DMA transfer is already active before modifying the
            /// buffer.
            ///
            /// The closure should write everything, returning the number of bytes loaded into the
            /// DMA buffer, or error
            fn write_fn<F: FnOnce(&mut [u8]) -> Result<usize, ()>>(
                &mut self,
                handle: &mut T,
                f: F,
            ) -> nb::Result<(), Error>;

            fn flush(&mut self, handle: &mut T) -> nb::Result<(), Error>;

            fn write(&mut self, handle: &mut T, bytes: &[u8]) -> nb::Result<(), Error> {
                let size = bytes.len();
                self.write_fn(handle, |buf| {
                    if size > buf.len() {
                        Err(())
                    } else {
                        buf[..size].copy_from_slice(bytes);
                        Ok(size)
                    }
                })
                .map(|_| ())
                .map_err(|_| nb::Error::Other(Error::BufferToSmall))
            }
        }
    }
}

pub mod i2c {

    pub trait Reset {
        fn reset(&mut self);
    }

    pub mod blocking {
        use crate::Timeout;
        pub use embedded_hal::blocking::{
            delay::DelayMs,
            i2c::{AddressMode, SevenBitAddress, TenBitAddress},
        };

        pub trait ErrorType {
            type Error: Error;
        }

        pub trait Error: core::fmt::Debug + defmt::Format {
            fn kind(&self) -> ErrorKind;
        }

        #[derive(Debug, PartialEq, Copy, Clone, defmt::Format)]
        pub enum ErrorKind {
            Bus,
            ArbitrationLost,
            Nack,
            InvalidAddress,
            TimedOut,
            Other,
        }

        impl Error for ErrorKind {
            fn kind(&self) -> ErrorKind {
                *self
            }
        }

        impl From<crate::TimeoutExpired> for ErrorKind {
            fn from(_: crate::TimeoutExpired) -> Self {
                Self::TimedOut
            }
        }

        pub trait WriteRead<A>: super::Reset + ErrorType
        where
            A: AddressMode,
        {
            fn write_read_with_timeout<'a, T>(
                &mut self,
                address: A,
                bytes: &[u8],
                buffer: &mut [u8],
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<(), Self::Error>
            where
                T: DelayMs<u32>;
        }
    }
}
