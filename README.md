This workspace provides two Rust crates used in personal and work projects.
They will likely never be published to crates.io and there no guarantess on version compatabilty.
They are public mostly for the small chance that some approaches or code may be useful to someomeone else.

- `stm32l0xx-hal-alt`: an alternative HAL for STM32L0 targets 
- `embedded-hal-ext`: additional traits for embedded targets.
