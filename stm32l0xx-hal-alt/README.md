# `stm32l0xx-hal-alt`: an alternative HAL for STM32L0

This crate provides an alternative to [stm32l0xx-hal](https://github.com/stm32-rs/stm32l0xx-hal) and focuses on type-state interfaces to peripherals.
It is not as complete as `stm32l0xx-hal` and likely never will be, nor does it expose interfaces at as high a level.
Instead, this package is intended to be a mid-level HAL that eases configuration of peripherals but ultimate control is left for the user.


## Key differences from `stm32l0xx-hal`
- Peripherals exposed as new types instead of extension traits
- Type-state alternate-function modes for GPIO pins (e.g. `PA0<AF4>`)
- Type-state expression of external-interrupt lines (e.g. `EXTI1<Enabled<Rising>>`)
- Type-state expression of DMA channels (e.g. `CH1<Disabled, ADC>`)
