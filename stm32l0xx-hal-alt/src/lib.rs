//! STM32L0xx Hardware Abstraction Layer
//!
//! Modeled after the 'official' [stm32l07xx-hal](https://crates.io/crates/stm32l0xx-hal) crate.
//! Not as complete, nor exposes as high a level.
//!
//! Code that targets the micro-controller but is general enough not to be tied to a specific
//! use case should be placed here
#![no_std]
//#![deny(warnings)]
//#![deny(missing_docs)]
#![deny(rust_2018_idioms)]

#[cfg(feature = "stm32l0x1")]
pub use stm32l0::stm32l0x1 as pac;

#[cfg(feature = "stm32l0x2")]
pub use stm32l0::stm32l0x2 as pac;

#[cfg(feature = "stm32l0x3")]
pub use stm32l0::stm32l0x3 as pac;

//pub mod delay;
pub mod adc;
pub mod delay;
pub mod dma;
pub mod exti;
pub mod flash;
pub mod gpio;
pub mod i2c;
pub mod pwr;
pub mod rcc;
pub mod rtc;
pub mod serial;
pub mod watchdog;

/// Private trait for borrowing an underlying peripheral register
///
/// Similar to the standard 'Borrow' trait, just with a different
/// name to be explicit.
///
/// Underlying implementations generally use unsafe for known-safe
/// changes to different parts of the same peripheral.  For example,
/// changing the enabled Rx-related interrupts on a UART after the
/// peripheral has already been split into Tx and Rx halves.
///
/// Safe use of this requires knowledge of the architecture beyond
/// what is possible just by Rust's borrow checker.
trait Borrow<T: ?Sized> {
    fn borrow(&self) -> &T;
}

trait BorrowMut<T: ?Sized>: Borrow<T> {
    fn borrow_mut(&mut self) -> &mut T;
}
