//! RS232 serial interface using DMA

#![allow(unused_imports)]
#![allow(dead_code)]

use crate::{
    dma::{
        channel::{
            target::{LPUART1_TX, USART1_TX, USART2_TX},
            FromMemory, FromPeripheral, OneShot, PeripheralChannel, BITS8, CH2, CH3, CH4, CH5, CH7,
        },
        transfer::Transfer,
        Dma1,
    },
    pac::{
        dma1,
        lpuart1::{ICR as ICRLP1, ISR as ISRLP1},
        usart1::{ICR as ICR1, ISR as ISR1},
        LPUART1, USART1,
    },
    rcc::Rcc,
    serial::Pins,
    Borrow,
};

use embedded_hal_ext::{
    serial::nb::{Error, WriteMany},
    DisablePeripheral, EnablePeripheral,
};

use core::{
    marker::PhantomData,
    ops::{Deref, DerefMut},
    pin::Pin,
    sync::atomic::{self, Ordering},
};

pub trait UartChannel<T, D>:
    PeripheralChannel<Target = T, Direction = D, TransferMode = OneShot, WordSize = BITS8>
{
}

impl UartChannel<USART1_TX, FromMemory> for CH2<USART1_TX, OneShot> {}
impl UartChannel<USART1_TX, FromMemory> for CH4<USART1_TX, OneShot> {}

impl UartChannel<USART2_TX, FromMemory> for CH4<USART2_TX, OneShot> {}

impl UartChannel<LPUART1_TX, FromMemory> for CH2<LPUART1_TX, OneShot> {}
impl UartChannel<LPUART1_TX, FromMemory> for CH7<LPUART1_TX, OneShot> {}

pub(crate) enum State<Ch, Buffer> {
    Ready {
        channel: Ch,
        buffer: Pin<Buffer>,
    },
    Active {
        transfer: Transfer<Ch, Buffer, crate::dma::transfer::Active>,
    },
}

/// Tx side of UART using DMA
pub struct Tx<U, T, Ch: UartChannel<T, FromMemory>, const N: usize> {
    /// DMA transfer state
    pub(crate) state: Option<State<Ch, &'static mut heapless::Vec<u8, N>>>,
    /// Tx half of uart
    pub(crate) tx: crate::serial::Tx<U>,
    pub(crate) _target: PhantomData<T>,
}

macro_rules! tx_ext {
    ($uart:ident, $target:ty, $isr:ident, $icr:ident) => {
        impl<Ch: UartChannel<$target, FromMemory>, const N: usize> EnablePeripheral
            for Tx<$uart, $target, Ch, N>
        {
            type Error = <crate::serial::Tx<$uart> as EnablePeripheral>::Error;

            fn enable(&mut self) -> Result<(), Self::Error> {
                self.tx.enable()
            }

            fn is_enabled(&self) -> Result<bool, Self::Error> {
                self.tx.is_enabled()
            }
        }

        impl<Ch: UartChannel<$target, FromMemory>, const N: usize> DisablePeripheral
            for Tx<$uart, $target, Ch, N>
        {
            fn disable(&mut self) -> Result<(), Self::Error> {
                self.tx.disable()
            }
        }

        impl<Ch: UartChannel<$target, FromMemory>, const N: usize> WriteMany<crate::dma::Dma1>
            for Tx<$uart, $target, Ch, N>
        {
            fn write_fn<F>(&mut self, handle: &mut crate::dma::Dma1, f: F) -> nb::Result<(), Error>
            where
                F: FnOnce(&mut [u8]) -> Result<usize, ()>,
            {
                if !self.tx.is_enabled().unwrap() {
                    defmt::warn!("{} is disabled", stringify! {$uart});
                    return Err(nb::Error::Other(Error::PeripheralDisabled));
                }

                let _ = self.flush(handle)?;

                let (mut channel, mut buffer) = match self.state.take().unwrap() {
                    State::Ready { channel, buffer } => (channel, buffer),
                    _ => unreachable! {"Active transfers should have finished due to flush() call"},
                };

                // Copy data into transmit buffer
                buffer.clear();
                buffer.resize_default(N).unwrap();
                match f(&mut buffer) {
                    Ok(len) => {
                        buffer.truncate(len);
                        channel.set_words_to_transfer(len as u16);
                    }
                    Err(_) => {
                        self.state = Some(State::Ready { channel, buffer });
                        return Err(nb::Error::Other(Error::Other));
                    }
                }

                self.tx.borrow().icr.write(|w| w.tccf().clear());
                let transfer = Transfer::new(channel, buffer).start(handle);
                self.state = Some(State::Active { transfer });
                Ok(())
            }

            fn flush(&mut self, handle: &mut crate::dma::Dma1) -> nb::Result<(), Error> {
                match self.state.take().unwrap() {
                    State::Ready { channel, buffer } => {
                        self.state = Some(State::Ready { channel, buffer });
                        Ok(())
                    }
                    State::Active { transfer } => {
                        if !transfer.is_complete(handle) {
                            self.state = Some(State::Active { transfer });
                            Err(nb::Error::WouldBlock)
                        } else {
                            let crate::dma::transfer::Resources { channel, buffer } =
                                transfer.wait(handle).free();
                            self.state = Some(State::Ready { channel, buffer });
                            Ok(())
                        }
                    }
                }
            }
        }
    };
}

tx_ext!(USART1, USART1_TX, ISR1, ICR1);
tx_ext!(LPUART1, LPUART1_TX, ISRLP1, ICRLP1);
