//! DMA Channel definitions
//!
//! The DMA peripheral channels can be used for hardware-controlled copying of data
//! between memory spaces or to and from peripherals.

use crate::{
    dma::{channel::target::ADC as ADC_TARGET, Dma1},
    pac::{
        dma1::{
            self,
            ch::cr::{CIRC_A, DIR_A, PSIZE_A},
        },
        ADC, I2C1, I2C2, LPUART1, USART1, USART2,
    },
    Borrow,
};
use core::convert::Infallible;
use embedded_hal_ext::{DisablePeripheral, EnablePeripheral};

use core::marker::PhantomData;

pub mod target {
    #![allow(non_camel_case_types)]

    /// Remap to ADC
    pub struct ADC;

    /// Remap to TIM2 CH3
    pub struct TIM2_CH3;

    /// REMAP to AES IN
    pub struct AES_IN;

    /// No peripheral remap
    pub struct NONE;

    /// Remap to SPI1 RX
    pub struct SPI1_RX;

    /// Remap to USART1 TX
    pub struct USART1_TX;

    /// Remap to I2C1_TX
    pub struct I2C1_TX;

    /// Remap to TIM2 UP
    pub struct TIM2_UP;

    /// Remap to TIM6 UP
    pub struct TIM6_UP;

    /// Remap to TIM3 CH3
    pub struct TIM3_CH3;

    /// Remap to AES OUT
    pub struct AES_OUT;

    /// Remap to USART4 RX
    pub struct USART4_RX;

    /// Remap to USART5 RX
    pub struct USART5_RX;

    /// Remap to I2C3 TX
    pub struct I2C3_TX;

    /// Remap to SPI1_TX
    pub struct SPI1_TX;

    /// Remap to USART1_RX
    pub struct USART1_RX;

    /// Remap to LPUART1_RX
    pub struct LPUART1_RX;

    /// Remap to I2C1_RX
    pub struct I2C1_RX;

    /// Remap to TIM2_CH2
    pub struct TIM2_CH2;

    /// Remap to TIM4_CH4_TI4_UP
    pub struct TIM4_CH4_TI4_UP;

    /// Remap to USART4_TX
    pub struct USART4_TX;

    /// Remap to USART5_TX
    pub struct USART5_TX;

    /// Remap to I2C3_RX
    pub struct I2C3_RX;

    /// Remap to SPI2_RX
    pub struct SPI2_RX;

    /// Remap to USART2_TX
    pub struct USART2_TX;

    /// Remap to I2C2_TX
    pub struct I2C2_TX;

    /// Remap to TIM2_CH4
    pub struct TIM2_CH4;

    /// Remap to TIM7_UP
    pub struct TIM7_UP;

    /// Remap to SPI2_TX
    pub struct SPI2_TX;

    /// Remap to USART2_RX
    pub struct USART2_RX;

    /// Remap to I2C2_RX
    pub struct I2C2_RX;

    /// Remap to TIM2_CH1
    pub struct TIM2_CH1;

    /// Remap to TIM3_CH1
    pub struct TIM3_CH1;

    /// Remap to TIM3_TRIG
    pub struct TIM3_TRIG;

    /// Remap to LPUART1_TX
    pub struct LPUART1_TX;

    /// Remap to TIM2_CH2_TIM2_CH4
    pub struct TIM2_CH2_TIM2_CH4;
}
use target::*;

/// DMA Channel Interrupt types
pub enum Interrupt {
    /// Transfer Error
    Error = 0b1000,
    /// Transfer half complete
    HalfTransfer = 0b0100,
    /// Transfer complete
    TransferComplete = 0b0010,
}

pub struct FromMemory {}
pub struct FromPeripheral {}

impl From<FromMemory> for DIR_A {
    fn from(_: FromMemory) -> Self {
        DIR_A::FromMemory
    }
}

impl From<FromPeripheral> for DIR_A {
    fn from(_: FromPeripheral) -> Self {
        DIR_A::FromPeripheral
    }
}

pub trait Word {
    type Pointer;
}

/// An 8-bit word
pub struct BITS8 {}
/// A 16-bit word
pub struct BITS16 {}
/// A 32-bit word
pub struct BITS32 {}

impl Word for BITS8 {
    type Pointer = *const u8;
}

impl Word for BITS16 {
    type Pointer = *const u16;
}

impl Word for BITS32 {
    type Pointer = *const u32;
}

impl From<BITS8> for PSIZE_A {
    fn from(_: BITS8) -> Self {
        PSIZE_A::Bits8
    }
}

impl From<BITS16> for PSIZE_A {
    fn from(_: BITS16) -> Self {
        PSIZE_A::Bits16
    }
}

impl From<BITS32> for PSIZE_A {
    fn from(_: BITS32) -> Self {
        PSIZE_A::Bits32
    }
}

/// OneShot DMA transfer mode
pub struct OneShot {}
/// Circular DMA transfer mode
pub struct Circular {}

impl From<OneShot> for CIRC_A {
    fn from(_: OneShot) -> Self {
        CIRC_A::Disabled
    }
}

impl From<Circular> for CIRC_A {
    fn from(_: Circular) -> Self {
        CIRC_A::Enabled
    }
}

/// DMA transfer mode
pub trait Mode {
    const MODE: CIRC_A;
}

impl Mode for OneShot {
    const MODE: CIRC_A = CIRC_A::Disabled;
}
impl Mode for Circular {
    const MODE: CIRC_A = CIRC_A::Enabled;
}

pub trait PeripheralChannel: EnablePeripheral + DisablePeripheral {
    type Target;
    type Direction;
    type TransferMode: Mode;
    type WordSize: Word;

    /// Set the memory address for the DMA transfer
    fn set_memory_address(&mut self, v: <Self::WordSize as Word>::Pointer);

    /// Get the memory address for the DMA transfer
    fn memory_address(&self) -> u32;

    /// Set number of words to transfer
    fn set_words_to_transfer(&mut self, words: u16);

    /// Check if the channel transfer has finished or errored
    ///
    /// Will not block and changes no status flags on the hardware channel peripheral.
    ///
    /// Completion is defined as either a full transfer complete or error condition on the
    /// channel.  Half transfers are not counted
    fn is_complete(&self, handle: &mut Dma1) -> bool;

    /// Wait for a channel transfer to complete
    ///
    /// Channel transfer status flags are cleared
    fn wait(&mut self, handle: &mut Dma1) -> nb::Result<(), Infallible>;
}

macro_rules! channels {
    ($(($CHi:ident, $chi:ident, $i:expr, $doc:expr),)+) => {

        $(
            #[doc=$doc]
            pub struct $CHi<T, M> {
                remap: core::marker::PhantomData<T>,
                mode: core::marker::PhantomData<M>,
            }

            impl<T, M> $CHi<T, M> {
                // /// Mask for transfer complete and transfer error on IFCR reg
                // const READ_COMPLETE_MASK: u32 = 0b1010 << 4 * ($i - 1);
                /// Mask for clearing all channel status flags thorugh IFCR global bit
                const WRITE_COMPLETE_MASK: u32 = 1 << 4 * ($i - 1);
            }

            impl<T, M> Borrow<dma1::CH> for $CHi<T, M> {
                fn borrow(&self) -> &dma1::CH {
                    &unsafe{&*(crate::pac::DMA1::PTR)}.$chi
                    // let base = crate::pac::DMA1::PTR as *const _ as u32;
                    // let addr = base + 0x08 + (Self::ID as u32 - 1) * 5 * 0x04;
                    // //defmt::trace!("ch reg: 0x{=u32:X}", addr);
                    // unsafe { &*(addr as *const u32 as *const dma1::CH) }
                }
            }

            // impl<T> BorrowMut<dma1::CH> for $CHi<T> {
            //     fn borrow_mut(&mut self) -> &mut dma1::CH {
            //         &mut (*(crate::pac::DMA1::PTR as *mut _)).$chi
            //         // let base = crate::pac::DMA1::PTR as *const _ as u32;
            //         // let addr = base + 0x08 + (Self::ID as u32 - 1) * 5 * 0x04;
            //         // unsafe { &mut *(addr as *mut u32 as *mut dma1::CH) }
            //     }
            // }

            impl <T, M> EnablePeripheral for $CHi<T, M> {
                type Error = core::convert::Infallible;

                fn enable(&mut self) -> Result<(), Self::Error>{
                    <Self as Borrow<dma1::CH>>::borrow(self).cr.modify(|_, w| w.en().enabled());
                    Ok(())
                }

                fn is_enabled(&self) -> Result<bool, Self::Error> {
                    Ok(<Self as Borrow<dma1::CH>>::borrow(self).cr.read().en().is_enabled())

                }

            }

            impl <T, M> DisablePeripheral for $CHi<T, M> {
                fn disable(&mut self) -> Result<(), Self::Error> {
                    <Self as Borrow<dma1::CH>>::borrow(self).cr.modify(|_, w| w.en().disabled());
                    unsafe {
                        (*crate::pac::DMA1::ptr()).ifcr.write(|w| w.bits(Self::WRITE_COMPLETE_MASK))
                    };
                    Ok(())
                }
            }
        )+
    };
}

channels! {
    (CH1, ch1, 1, "DMA1 channel 1"),
    (CH2, ch2, 2, "DMA1 channel 2"),
    (CH3, ch3, 3, "DMA1 channel 3"),
    (CH4, ch4, 4, "DMA1 channel 4"),
    (CH5, ch5, 5, "DMA1 channel 5"),
    (CH6, ch6, 6, "DMA1 channel 6"),
    (CH7, ch7, 7, "DMA1 channel 7"),
}

macro_rules! impl_select {
    (
        ($fn: ident, $target:ident, $pac: ident, $reg: ident, $dir: ident, $wordsize: ident): {
            $(($CHi: ident, $cis:ident, $chi: ident, $i:expr, $mapping:ident),)+
        }
    ) => {
        $(
        impl<T, M> $CHi<T, M> {
            pub fn $fn<N: Mode>(mut self) -> $CHi<$target, N> {
                self.disable().ok();
                unsafe{
                    let dma = &*crate::pac::DMA1::PTR;
                    // set mapping
                    dma.cselr.modify(|_, w| w.$cis().$mapping());
                    // set peripheral address
                    let addr = &(*$pac::PTR).$reg as *const _ as u32;
                    dma.$chi.par.write(|w| w.bits(addr));
                    // set transfer options
                    dma.$chi.cr.modify(|_, w| w.mem2mem().disabled()
                    .pinc().disabled()
                    .minc().enabled()
                    .circ().variant(N::MODE)
                    .msize().variant(Into::into($wordsize{}))
                    .psize().variant(Into::into($wordsize{}))
                    .dir().variant(Into::into($dir{})));
                }
                $CHi {remap: PhantomData{}, mode: PhantomData{}}
            }
        }

        impl<M: Mode> PeripheralChannel for $CHi<$target, M>{
            type Target=$target;
            type Direction=$dir;
            type TransferMode=M;
            type WordSize=$wordsize;

            fn set_memory_address(&mut self, v: <Self::WordSize as Word>::Pointer) {
                unsafe{
                   let dma = &*crate::pac::DMA1::PTR;
                   //dma.$chi.mar.write(|w| w.bits(v.as_ptr() as u32));
                   dma.$chi.mar.write(|w| w.bits(v as u32));
                }
            }

            /// Set the memory address for the DMA transfer
            fn memory_address(&self) -> u32 {
                unsafe {
                let dma = &*crate::pac::DMA1::PTR;
                dma.$chi.mar.read().ma().bits()
    }
            }

            /// Set number of words to transfer
            fn set_words_to_transfer(&mut self, words: u16) {
                unsafe {
                let dma = &*crate::pac::DMA1::PTR;
                dma.$chi.ndtr.write(|w| w.ndt().bits(words));
    }
                defmt::trace!("ntdr: 0x{=u16:X}", words);
            }

            /// Check if the channel transfer has finished or errored
            fn is_complete(&self, handle: &mut Dma1) -> bool {

                match self.is_enabled() {
                    Ok(true) => {},
                    _ => {return true;}
                }

                const READ_MASK: u32 = 0b1010 << (4 * ($i - 1));
                //const WRITE_MASK: u32 = 1 << (4 * ($i - 1));

                unsafe {handle.with_raw(|dma| (dma.isr.read().bits() & READ_MASK) != 0)}
            }

            fn wait(&mut self, handle: &mut Dma1) -> nb::Result<(), core::convert::Infallible> {
                if !self.is_complete(handle) {
                    Err(nb::Error::WouldBlock)
                }
                else {
                    unsafe {handle.with_raw(|dma| dma.ifcr.write(|w| w.bits(Self::WRITE_COMPLETE_MASK)))};
                    Ok(())
                }
            }

        }
        )+
    }
}

impl_select! {
    (select_adc, ADC_TARGET, ADC, dr, FromPeripheral, BITS16) : {
        (CH1, c1s, ch1, 1, no_mapping),
        (CH2, c2s, ch2, 2, no_mapping),
    }
}

impl_select! {
    (select_usart1_tx, USART1_TX, USART1, tdr, FromMemory, BITS8): {
        (CH2, c2s, ch2, 2, map3),
        (CH4, c4s, ch4, 4, map3),
    }
}

impl_select! {
    (select_usart1_rx, USART1_RX, USART1, rdr, FromPeripheral, BITS8): {
        (CH3, c3s, ch3, 3, map3),
        (CH5, c5s, ch5, 5, map3),
    }
}

impl_select! {
    (select_usart2_tx, USART2_TX, USART2, tdr, FromMemory, BITS8): {
        (CH4, c4s, ch4, 4, map4),
    }
}

impl_select! {
    (select_usart2_rx, USART2_RX, USART2, rdr, FromPeripheral, BITS8): {
        (CH5, c5s, ch5, 5, map4),
    }
}

impl_select! {
    (select_lpuart1_tx, LPUART1_TX, LPUART1, tdr, FromMemory, BITS8): {
        (CH2, c2s, ch2, 2, map5),
        (CH7, c7s, ch7, 7, map5),
    }
}

impl_select! {
    (select_lpuart1_rx, LPUART1_RX, LPUART1, rdr, FromPeripheral, BITS8): {
        (CH3, c3s, ch3, 3, map5),
        (CH6, c6s, ch6, 6, map5),
    }
}

impl_select! {
    (select_i2c1_tx, I2C1_TX, I2C1, txdr, FromMemory, BITS8): {
        (CH2, c2s, ch2, 2, map6),
        (CH6, c6s, ch6, 6, map6),
    }
}

impl_select! {
    (select_i2c1_rx, I2C1_RX, I2C1, rxdr, FromPeripheral, BITS8): {
        (CH3, c3s, ch3, 3, map6),
        (CH7, c7s, ch7, 7, map6),
    }
}

impl_select! {
    (select_i2c2_tx, I2C2_TX, I2C2, txdr, FromMemory, BITS8): {
        (CH4, c4s, ch4, 4, map7),
    }
}

impl_select! {
    (select_i2c2_rx, I2C2_RX, I2C2, rxdr, FromPeripheral, BITS8): {
        (CH5, c5s, ch5, 5, map7),
    }
}

// impl<T> CH1<T> {
//     pub fn remap_adc(mut self) -> CH1<remap::ADC> {
//         self.disable().ok();
//         &(*crate::pac::DMA1::PTR).cselr.modify(|_, w| w.c1s().no_mapping());
//         CH1 {remap: PhantomData{}}
//     }
// }

// impl<T> From<CH2<T>> for CH2<remap::USART1_TX> {
//     fn from(mut channel: CH2<T>) -> Self {
//
//         channel.set_peripheral_address(unsafe { &(*USART1::PTR).tdr as *const _ as u32}, handle);
//
//         #[rustfmt::skip]
//         channel.borrow().cr.modify(|_, w| {
//             w.mem2mem().disabled()
//                 .msize().bits8()
//                 .psize().bits8()
//                 .pinc().disabled()
//                 .minc().enabled()
//                 .circ().disabled()
//                 .dir().from_memory()
//         });
//     }
// }

/// Channel mappings after reset
pub struct DefaultChannels {
    /// DMA1 Channel 1
    pub ch1: CH1<ADC, OneShot>,
    /// DMA1 Channel 2
    pub ch2: CH2<ADC, OneShot>,
    /// DMA1 Channel 3
    pub ch3: CH3<NONE, OneShot>,
    /// DMA1 Channel 4
    pub ch4: CH4<NONE, OneShot>,
    /// DMA1 Channel 5
    pub ch5: CH5<NONE, OneShot>,
    /// DMA1 Channel 6
    pub ch6: CH6<NONE, OneShot>,
    /// DMA1 Channel 7
    pub ch7: CH7<NONE, OneShot>,
}
// use crate::dma::channel::remap::{CS1, CS2, CS3, CS4, CS5, CS6, CS7};

impl DefaultChannels {
    pub(crate) unsafe fn take() -> Self {
        Self {
            ch1: CH1 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch2: CH2 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch3: CH3 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch4: CH4 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch5: CH5 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch6: CH6 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
            ch7: CH7 {
                remap: PhantomData {},
                mode: PhantomData {},
            },
        }
    }
}

// impl<T> CH2<T> {
//     pub fn into_usart1_tx(self, handle: &mut Dma1) -> CH2<remap::USART1_TX> {
//         let mut channel = self.remap::<remap::USART1_TX>(handle);
//         crate::dma::serial::configure_dma_channel_for_uart(channel.channel(), handle);
//         channel
//     }
//
//     pub fn into_lpuart1_tx(self, handle: &mut Dma1) -> CH2<remap::LPUART1_TX> {
//         let mut channel = self.remap::<remap::LPUART1_TX>(handle);
//         crate::dma::serial::configure_dma_channel_for_uart(channel.channel(), handle);
//         channel
//     }
// }
//
// impl<T> CH4<T> {
//     pub fn into_usart1_tx(self, handle: &mut Dma1) -> CH4<remap::USART1_TX> {
//         let mut channel = self.remap::<remap::USART1_TX>(handle);
//         crate::dma::serial::configure_dma_channel_for_uart(channel.channel(), handle);
//         channel
//     }
// }
//
// impl<T> CH7<T> {
//     pub fn into_lpuart1_tx(self, handle: &mut Dma1) -> CH7<remap::LPUART1_TX> {
//         let mut channel = self.remap::<remap::LPUART1_TX>(handle);
//         crate::dma::serial::configure_dma_channel_for_uart(channel.channel(), handle);
//         channel
//     }
// }
