//! DMA transfers

pub mod adc;
pub mod channel;
pub mod serial;
pub mod transfer;

use crate::{pac::DMA1, rcc::Rcc};

/// DMA peripheral
pub struct Dma1 {
    dma1: DMA1,
}

impl Dma1 {
    /// Enable the DMA1 peripheral
    pub fn constrain(dma1: DMA1, rcc: &mut Rcc) -> (Self, channel::DefaultChannels) {
        rcc.rcc.ahbrstr.modify(|_, w| w.dmarst().reset());
        rcc.rcc.ahbrstr.modify(|_, w| w.dmarst().clear_bit());
        rcc.rcc.ahbenr.modify(|_, w| w.dmaen().enabled());

        // Safe because we just reset the peripheral
        let channels = unsafe { channel::DefaultChannels::take() };

        (Self { dma1 }, channels)
    }

    /// Return the underlying DMA1 peripheral
    pub fn free(self) -> DMA1 {
        self.dma1
    }

    /// Gain access to the underlying DMA1 peripherial
    ///
    /// As this HAL doesn't provide full high-level access ot all
    /// peripherals the user may need access to the RCC for their own
    /// configuration/setup needs.
    ///
    /// # Safety
    /// Modifying the DMA1 in this manor carries the risk of invalidating
    /// all other assumptions this HAL provides.  Users should be *extremely*
    /// cautious when manipulating the bare DMA1 registers.
    pub unsafe fn with_raw<F, T>(&mut self, f: F) -> T
    where
        F: FnOnce(&DMA1) -> T,
    {
        f(&self.dma1)
    }
}
