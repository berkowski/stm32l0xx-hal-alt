//! DMA Transfers

use core::{marker::PhantomData, pin::Pin};

/// Resources used in the DMA transfer
pub struct Resources<C, T> {
    /// DMA channel
    pub channel: C,
    /// DMA buffer
    pub buffer: Pin<T>,
}

/// Transfer Error
pub enum Error {}

/// Result type for DMA transfer
pub type Result<C, T> = core::result::Result<Resources<C, T>, (Resources<C, T>, Error)>;

/// Ready state
pub struct Ready {}

/// Active state
pub struct Active {}

/// DMA transfer structure
pub struct Transfer<Channel, Buffer, State> {
    resources: Resources<Channel, Buffer>,
    state: PhantomData<State>,
}

// impl<Channel, Buffer, T> Transfer<Channel, Buffer, T>
// where
//     Channel: crate::dma::channel::Channel,
// {
//     // const CHANNEL_SHIFT: u8 = (Channel::ID - 1) * 4;
//
//     fn channel_register(_handle: &mut crate::dma::Dma1) -> &'static crate::pac::dma1::CH {
//         let base = crate::pac::DMA1::ptr() as *const _ as u32;
//         let addr = base + 0x08 + (Channel::ID as u32 - 1) * 5 * 0x04;
//         defmt::trace!("ch reg: {=u32}", addr);
//         unsafe { &*(addr as *const u32 as *const crate::pac::dma1::CH) }
//     }
// }

impl<Channel, Buffer> Transfer<Channel, Buffer, Ready>
where
    Channel: crate::dma::channel::PeripheralChannel,
{
    /// Create a new DMA transfer using the provided channel and buffer
    pub fn new(channel: Channel, buffer: Pin<Buffer>) -> Self {
        Self {
            resources: Resources { channel, buffer },
            state: PhantomData {},
        }
    }

    /// Start the DMA transfer
    pub fn start(self, _handle: &mut crate::dma::Dma1) -> Transfer<Channel, Buffer, Active> {
        let Resources {
            mut channel,
            buffer,
        } = self.resources;
        channel.enable().ok();
        Transfer {
            resources: Resources { channel, buffer },
            state: PhantomData {},
        }
    }

    /// Return the DMA channel and buffer resources for reuse
    pub fn free(self) -> Resources<Channel, Buffer> {
        self.resources
    }
}

impl<Channel, Buffer> Transfer<Channel, Buffer, Active>
where
    Channel: crate::dma::channel::PeripheralChannel,
{
    /// Cancel an active DMA transfer
    pub fn cancel(self, _handle: &mut crate::dma::Dma1) -> Transfer<Channel, Buffer, Ready> {
        let Resources {
            mut channel,
            buffer,
        } = self.resources;
        channel.disable().ok();
        Transfer {
            resources: Resources { channel, buffer },
            state: PhantomData {},
        }
    }

    /// Check if the active transfer is complete
    pub fn is_complete(&self, handle: &mut crate::dma::Dma1) -> bool {
        self.resources.channel.is_complete(handle)
    }

    /// Wait for an active transfer to finish
    ///
    /// The transfer is returned in the Ready state.
    pub fn wait(mut self, handle: &mut crate::dma::Dma1) -> Transfer<Channel, Buffer, Ready> {
        nb::block!(self.resources.channel.wait(handle)).unwrap();
        self.cancel(handle)
    }
}
// /// DMA Transfer
// pub enum Transfer<T, D, E>
//     where T: core::convert::AsMut<[u8]> + 'static
// {
//     /// DMA Transfer ready to start
//     Ready{
//         /// DMA Channel
//         ch: D,
//         /// DMA Buffer
//         buffer: Pin<&'static mut T>
//     },
//     /// DMA Transfer is active
//     Active{
//         /// DMA Channel
//         ch: E,
//         /// DMA Buffer
//         buffer: Pin<&'static mut T>
//     },
// }
