//! Using DMA with ADC

use crate::{
    adc::{Interrupt, Ready, Started},
    dma::channel::{self, Mode, PeripheralChannel},
    pac::{adc::cfgr1::DMACFG_A, dma1::ch::cr::CIRC_A},
};

use core::{ops::Deref, pin::Pin};

use embedded_hal_ext::{DisablePeripheral, EnablePeripheral};

pub struct Adc<Ch, State, const N: usize> {
    adc: crate::adc::Adc<State, N>,
    channel: Ch,
    buffer: Pin<&'static mut [u16; N]>,
}

impl<Ch, State, const N: usize> Adc<Ch, State, N>
where
    Ch: channel::PeripheralChannel<
        Target = channel::target::ADC,
        Direction = channel::FromPeripheral,
        WordSize = channel::BITS16,
    >,
{
    /// Gain access to the underlying ADC peripherial
    ///
    /// As this HAL doesn't provide full high-level access ot all
    /// peripherals the user may need access to the ADC for their own
    /// configuration/setup needs.
    ///
    /// # Safety
    /// Modifying the ADC in this manner carries the risk of invalidating
    /// all other assumptions this HAL provides.  Users should be *extremely*
    /// cautious when manipulating the bare ADC registers.
    pub unsafe fn with_raw<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&crate::pac::ADC) -> R,
    {
        self.adc.with_raw(|adc| f(adc))
    }

    /// Get active interrupts
    #[inline(always)]
    pub fn interrupts(&self) -> Interrupt {
        self.adc.interrupts()
    }

    /// Check for pending events
    ///
    /// Does not clear events, to do so use clear()
    #[inline(always)]
    pub fn pending(&self) -> Option<Interrupt> {
        self.adc.pending()
    }

    /// Clear pending events
    #[inline(always)]
    pub fn clear(&mut self, interrupts: Interrupt) {
        self.adc.clear(interrupts)
    }

    /// Get current buffer values
    pub fn buffer(&self) -> &[u16; N] {
        &self.buffer
    }
}

impl<Ch, const N: usize> Adc<Ch, Ready, N>
where
    Ch: channel::PeripheralChannel<
        Target = channel::target::ADC,
        Direction = channel::FromPeripheral,
        WordSize = channel::BITS16,
    >,
{
    pub fn from(
        adc: crate::adc::Adc<Ready, N>,
        mut channel: Ch,
        buffer: Pin<&'static mut [u16; N]>,
    ) -> Self {
        // disable ADC (?)
        // adc.disable();

        // Enable dma & set to one-shot

        adc.adc.cfgr1.modify(|_, w| {
            w.dmaen().enabled().dmacfg().variant(
                match <Ch as PeripheralChannel>::TransferMode::MODE {
                    CIRC_A::Enabled => DMACFG_A::Circular,
                    CIRC_A::Disabled => DMACFG_A::OneShot,
                },
            )
        });

        channel.set_memory_address(buffer.deref().as_slice().as_ptr());
        channel.set_words_to_transfer(N as u16);
        channel.enable().ok();

        Self {
            adc,
            channel,
            buffer,
        }
    }
}

impl<Ch, const N: usize> EnablePeripheral for Adc<Ch, Ready, N>
where
    Ch: channel::PeripheralChannel<
        Target = channel::target::ADC,
        Direction = channel::FromPeripheral,
        WordSize = channel::BITS16,
    >,
{
    type Error = core::convert::Infallible;

    fn enable(&mut self) -> Result<(), Self::Error> {
        self.adc.enable()
    }

    fn is_enabled(&self) -> Result<bool, Self::Error> {
        self.adc.is_enabled()
    }
}
impl<Ch, const N: usize> DisablePeripheral for Adc<Ch, Ready, N>
where
    Ch: channel::PeripheralChannel<
        Target = channel::target::ADC,
        Direction = channel::FromPeripheral,
        WordSize = channel::BITS16,
    >,
{
    fn disable(&mut self) -> Result<(), Self::Error> {
        self.adc.disable()
    }
}

impl<Ch, const N: usize> Adc<Ch, Ready, N> {
    /// Set active interrupts
    #[inline(always)]
    pub fn enable_interrupts(&mut self, interrupts: Interrupt) {
        self.adc.enable_interrupts(interrupts)
    }

    #[inline(always)]
    pub fn disable_interrupts(&mut self, interrupts: Interrupt) {
        self.adc.disable_interrupts(interrupts)
    }

    /// Start ADC conversion
    pub fn start(self) -> Adc<Ch, Started, N> {
        let Self {
            adc,
            channel,
            buffer,
        } = self;

        let adc: crate::adc::Adc<Started, N> = adc.start();

        Adc {
            adc,
            channel,
            buffer,
        }
    }
}

impl<Ch, const N: usize> Adc<Ch, Started, N> {
    /// Stop ADC conversion
    ///
    /// Stops any pending conversion and resets ADC and dma transfer channel.
    pub fn stop(self) -> Adc<Ch, Ready, N> {
        let Self {
            adc,
            channel,
            buffer,
        } = self;
        let adc: crate::adc::Adc<Ready, N> = adc.stop();
        Adc {
            adc,
            channel,
            buffer,
        }
    }
}
