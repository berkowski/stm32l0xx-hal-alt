//! RS232 serial port implementations

#![allow(dead_code)]

use crate::{
    dma::channel::{
        target::{LPUART1_TX, USART1_TX, USART2_TX},
        FromMemory,
    },
    gpio::{
        gpioa::{PA10, PA13, PA14, PA15, PA2, PA3, PA9},
        gpiob::{PB10, PB11},
        gpioc::{PC0, PC1, PC10, PC11, PC4, PC5},
        gpiod::{PD5, PD6, PD8, PD9},
        AF0, AF2, AF4, AF5, AF6, AF7,
    },
    pac::{lpuart1, usart1, LPUART1, USART1, USART2},
    rcc::Rcc,
    Borrow,
};

use embedded_hal::serial::{Read, Write};
use embedded_hal_ext::{DisablePeripheral, EnablePeripheral};
pub use embedded_time::rate::{Baud, Hertz};

use core::{marker::PhantomData, ops::Deref, pin::Pin};

macro_rules! uart_pins {
    ($($uart:ident: tx($tx:ident,$tx_af:ident) rx($rx:ident, $rx_af:ident))+) => {
        mod pins {
            use super::*;
            pub trait Sealed {}

            $(
            impl Sealed for ($tx<$tx_af>, $rx<$rx_af>) {}
            )+
        }

        /// Valid Tx and Rx pins for each UART peripheral
        pub trait Pins<T> : pins::Sealed {}

        $(
        impl Pins<$uart> for ($tx<$tx_af>, $rx<$rx_af>) {}
        )+
    };
}

uart_pins! {
    USART1: tx(PA9, AF4) rx(PA10, AF4)
    USART2: tx(PA2, AF4) rx(PA3, AF4)
    USART2: tx(PA14, AF4) rx(PA15, AF4)
    LPUART1: tx(PA14, AF5) rx(PA13, AF5)
    LPUART1: tx(PA2, AF5) rx(PA3, AF5)
    LPUART1: tx(PB10, AF4) rx(PB11, AF4)
    LPUART1: tx(PB11, AF7) rx(PB10, AF7)
    LPUART1: tx(PC10, AF0) rx(PC11, AF0)
    LPUART1: tx(PC4, AF2) rx(PC5, AF2)
    LPUART1: tx(PC1, AF6) rx(PC0, AF6)
    USART2: tx(PD5, AF0) rx(PD6, AF0)
    LPUART1: tx(PD8, AF0) rx(PD9, AF0)
}

/// Serial Port Error Kinds
pub enum Error {
    /// Overrun Error
    Overrun,
    /// Framing error - may indicate reception of a break character
    Framing,
    /// Noise Error
    Noise,
    /// Peripheral is not enabled
    NotEnabled,
}

bitflags::bitflags! {
/// UART interrupts
pub struct Interrupt: u8 {
    /// Overrun Error
    const OVERRUN = 1 << 0;
    /// Noise Error
    const NOISE = 1 << 1;
    /// Framing Error (may indicate reception of a break character)
    const FRAMING = 1 << 2;
    /// Idle line detection
    const IDLE = 1 << 3;
    /// Parity Error
    const PARITY = 1 << 4;
    /// Transmission Complete
    const TRANSMIT_COMPLETE = 1 << 5;
    /// Transmit buffer is empty and ready for the next byte
    const TRANSMIT_EMPTY = 1 << 6;
    /// Receve buffer not empty
    const RECEIVE_NOT_EMPTY = 1 << 7;
}
}

/// Tx side of uart using interrupts
pub struct Tx<T> {
    _uart: core::marker::PhantomData<T>,
}

/// Rx side of uart using interrupts
pub struct Rx<T> {
    _uart: core::marker::PhantomData<T>,
}

/// Basic Serial Port interface.
pub struct Serial<T, P> {
    pub(crate) uart: T,
    pclk_hz: Hertz<u32>,
    pins: P,
}

macro_rules! uart {
    ($uart:ident: {
        borrow: $borrow:ty,
        reg: ($apbXrstr:ident, $uartXrst:ident, $apbXenr:ident, $uartXen:ident, $apbX_clk:ident),
        dma: ($tx_dma_target:ident),
    }) => {
        impl<P> Serial<$uart, P>
        where
            P: Pins<$uart>,
        {
            /// Create a serial port
            ///
            /// The UART is enabled and the given baud rate is set.  No interrupts
            /// are enabled.
            pub fn new(uart: $uart, pins: P, baudrate: Baud, rcc: &mut Rcc) -> Self {
                rcc.rcc.$apbXrstr.modify(|_, w| w.$uartXrst().reset());
                rcc.rcc.$apbXrstr.modify(|_, w| w.$uartXrst().clear_bit());
                rcc.rcc.$apbXenr.modify(|_, w| w.$uartXen().enabled());

                uart.cr1.modify(|_, w| w.ue().disabled());

                uart.cr1.modify(|_, w| {
                    w.m0()
                        .bit8()
                        .m1()
                        .clear_bit()
                        .pce()
                        .disabled()
                        .rxneie()
                        .enabled()
                        .te()
                        .enabled()
                        .re()
                        .enabled()
                });

                uart.cr2.modify(|_, w| w.stop().stop1());
                let pclk_hz = rcc.clocks.$apbX_clk();

                // Set baud rate
                let brr = if uart.cr1.read().over8().is_oversampling8() {
                    let div = (2 * pclk_hz.0 / baudrate.0) as u16;
                    (div & 0xFFF0) + ((div & 0x000F) >> 1)
                } else {
                    (pclk_hz.0 / baudrate.0) as u16
                };

                uart.brr.write(|w| w.brr().bits(brr.into()));

                // Enable
                uart.cr1.modify(|_, w| w.ue().enabled());

                Self {
                    uart,
                    pclk_hz,
                    pins,
                }
            }

            /// Return the peripherals for reuse elsewhere
            pub fn free(self) -> ($uart, P) {
                (self.uart, self.pins)
            }

            /// Enable an interrupt
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    self.uart.cr3.modify(|_, w| w.eie().enabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    self.uart.cr1.modify(|_, w| w.peie().enabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    self.uart.cr1.modify(|_, w| w.idleie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.txeie().enabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.rxneie().enabled());
                }
            }

            /// Disable an interrupt
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    self.uart.cr3.modify(|_, w| w.eie().disabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    self.uart.cr1.modify(|_, w| w.peie().disabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    self.uart.cr1.modify(|_, w| w.idleie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.txeie().disabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.rxneie().disabled());
                }
            }

            /// Split the serial port into interrupt-driven Tx and Rx halves
            #[inline(always)]
            pub fn split(self) -> (Tx<$uart>, Rx<$uart>) {
                (
                    Tx {
                        _uart: core::marker::PhantomData,
                    },
                    Rx {
                        _uart: core::marker::PhantomData,
                    },
                )
            }
            // /// Get the Serial port baudrate
            // pub fn baudrate(&self) -> BaudRate {
            //     let brr = self.uart.brr.read().brr().bits();
            //     if self.uart.cr1.read().over8().is_oversampling8() {
            //         let div = ((brr & 0xFFF0) + ((brr & 0x000F) << 1)) as u32;
            //         BaudRate::from(2 * self.pclk_hz / div)
            //     } else {
            //         BaudRate::from(self.pclk_hz / (brr as u32))
            //     }
            // }

            // /// Set the Serial port baudrate
            // pub fn set_baudrate(&mut self, baud_rate: BaudRate) {
            //     let brr = if self.uart.cr1.read().over8().is_oversampling8() {
            //         let div = (2 * self.pclk_hz / baud_rate.0) as u16;
            //         (div & 0xFFF0) + ((div & 0x000F) >> 1)
            //     } else {
            //         (self.pclk_hz / baud_rate.0) as u16
            //     };

            //     self.uart.brr.write(|w| w.brr().bits(brr.into()));
            // }
        }

        impl<P> Read<u8> for Serial<$uart, P>
        where
            P: Pins<$uart>,
        {
            type Error = Error;

            fn read(&mut self) -> nb::Result<u8, Self::Error> {
                let isr = self.uart.isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.ore().bit_is_set() {
                    self.uart.icr.write(|w| w.orecf().clear());
                    Err(nb::Error::Other(Error::Overrun))
                } else if isr.nf().bit_is_set() {
                    self.uart.icr.write(|w| w.ncf().clear());
                    Err(nb::Error::Other(Error::Noise))
                } else if isr.fe().bit_is_set() {
                    self.uart.icr.write(|w| w.fecf().clear());
                    Err(nb::Error::Other(Error::Framing))
                } else if isr.rxne().bit_is_set() {
                    let byte = (self.uart.rdr.read().rdr().bits() & 0xFF) as u8;
                    Ok(byte)
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }
        impl<P> Write<u8> for Serial<$uart, P>
        where
            P: Pins<$uart>,
        {
            type Error = Error;

            fn write(&mut self, byte: u8) -> nb::Result<(), Self::Error> {
                let isr = self.uart.isr.read();
                if !isr.teack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.txe().bit_is_set() {
                    self.uart.tdr.write(|w| w.tdr().bits(byte.into()));
                    Ok(())
                } else {
                    return Err(nb::Error::WouldBlock);
                }
            }

            fn flush(&mut self) -> nb::Result<(), Self::Error> {
                let isr = self.uart.isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.tc().bit_is_set() {
                    Ok(())
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }

        impl EnablePeripheral for Rx<$uart> {
            type Error = core::convert::Infallible;

            #[inline(always)]
            fn enable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.re().enabled());
                Ok(())
            }

            #[inline(always)]
            fn is_enabled(&self) -> Result<bool, Self::Error> {
                Ok(self.borrow().cr1.read().re().is_enabled())
            }
        }

        impl DisablePeripheral for Rx<$uart> {
            #[inline(always)]
            fn disable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.re().disabled());
                Ok(())
            }
        }

        impl Rx<$uart> {
            /// Return true if receiver is enabled
            /// Enable an interrupt
            ///
            /// Ignores interrupt types not related to reception
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    uart.cr3.modify(|_, w| w.eie().enabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    uart.cr1.modify(|_, w| w.peie().enabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    uart.cr1.modify(|_, w| w.idleie().enabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    uart.cr1.modify(|_, w| w.rxneie().enabled());
                }
            }

            /// Disable an interrupt
            ///
            /// Ignores interrupt types not related to reception
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    uart.cr3.modify(|_, w| w.eie().disabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    uart.cr1.modify(|_, w| w.peie().disabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    uart.cr1.modify(|_, w| w.idleie().disabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    uart.cr1.modify(|_, w| w.rxneie().enabled());
                }
            }
        }

        impl Borrow<$borrow> for Rx<$uart> {
            fn borrow(&self) -> &$borrow {
                unsafe { &(*$uart::ptr()) }
            }
        }

        impl Read<u8> for Rx<$uart> {
            type Error = Error;

            fn read(&mut self) -> nb::Result<u8, Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.ore().bit_is_set() {
                    self.borrow().icr.write(|w| w.orecf().clear());
                    Err(nb::Error::Other(Error::Overrun))
                } else if isr.nf().bit_is_set() {
                    self.borrow().icr.write(|w| w.ncf().clear());
                    Err(nb::Error::Other(Error::Noise))
                } else if isr.fe().bit_is_set() {
                    self.borrow().icr.write(|w| w.fecf().clear());
                    Err(nb::Error::Other(Error::Framing))
                } else if isr.rxne().bit_is_set() {
                    let byte = (self.borrow().rdr.read().rdr().bits() & 0xFF) as u8;
                    Ok(byte)
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }

        impl EnablePeripheral for Tx<$uart> {
            type Error = core::convert::Infallible;

            /// Enable Transmitter
            #[inline(always)]
            fn enable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.te().enabled());
                Ok(())
            }

            /// Return true if transmitter is enabled
            #[inline(always)]
            fn is_enabled(&self) -> Result<bool, Self::Error> {
                Ok(self.borrow().cr1.read().te().is_enabled())
            }
        }

        impl DisablePeripheral for Tx<$uart> {
            #[inline(always)]
            fn disable(&mut self) -> Result<(), Self::Error> {
                let uart = self.borrow();
                // Wait for pending transmissions to clear before disabling
                while !uart.isr.read().tc().bit_is_set() {}
                uart.cr1.modify(|_, w| w.te().disabled());
                Ok(())
            }
        }

        impl Tx<$uart> {
            /// Enable an interrupt
            ///
            /// Ignores interrupts not related to transmission
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    uart.cr1.modify(|_, w| w.txeie().enabled());
                }
            }

            /// Disable an interrupt
            ///
            /// Ignores interrupts not related to transmission
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    uart.cr1.modify(|_, w| w.tcie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    uart.cr1.modify(|_, w| w.txeie().disabled());
                }
            }

            pub fn into_dma<Ch, const N: usize>(
                self,
                buffer: Pin<&'static mut heapless::Vec<u8, N>>,
                mut channel: Ch,
                _handle: &mut crate::dma::Dma1,
            ) -> crate::dma::serial::Tx<$uart, $tx_dma_target, Ch, N>
            where
                Ch: crate::dma::serial::UartChannel<$tx_dma_target, FromMemory>,
            {
                channel.set_memory_address(buffer.deref().as_slice().as_ptr());

                // Enable DMA for Transmit and error interrupts.
                let uart = self.borrow();

                // Wait for end of transmission
                while !uart.isr.read().tc().bit_is_set() {}

                uart.cr3.modify(|_, w| w.eie().enabled().dmat().enabled());

                crate::dma::serial::Tx {
                    state: Some(crate::dma::serial::State::Ready { channel, buffer }),
                    tx: self,
                    _target: PhantomData {},
                }
            }

            // pub fn into_dma_tx<T, Ch: Channel + Remap<T>, const N: usize> (
            //     self,
            //     buffer: Pin<&'static mut heapless::Vec<u8, N>>,
            //     mut channel: Ch,
            //     handle: &mut crate::dma::Dma1,
            //     ) -> crate::dma::serial::Tx<$uart, T, Ch, N>
            // {
            //     let mut channel = channel.remap::<T>(handle);

            //     channel.set_peripheral_address(unsafe { &(*$uart::PTR).tdr as *const _ as u32}, handle);
            //     channel.set_memory_address(buffer.deref().as_slice().as_ptr() as u32, handle);

            //     #[rustfmt::skip]
            //     channel.borrow().cr.modify(|_, w| {
            //         w.mem2mem().disabled()
            //             .msize().bits8()
            //             .psize().bits8()
            //             .pinc().disabled()
            //             .minc().enabled()
            //             .circ().disabled()
            //             .dir().from_memory()
            //     });

            //     // Enable DMA for Transmit and error interrupts.
            //     let uart = self.borrow();

            //     // Wait for end of transmission
            //     while !uart.isr.read().tc().bit_is_set() {}

            //     uart.cr3.modify(|_, w| w.eie().enabled().dmat().enabled());

            //     crate::dma::serial::Tx {
            //         state: Some(crate::dma::serial::State::Ready { channel, buffer }),
            //         tx: self,
            //     }
            // }
        }

        impl Borrow<$borrow> for Tx<$uart> {
            fn borrow(&self) -> &$borrow {
                unsafe { &(*$uart::ptr()) }
            }
        }

        impl Write<u8> for Tx<$uart> {
            type Error = Error;

            fn write(&mut self, byte: u8) -> nb::Result<(), Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.teack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.txe().bit_is_set() {
                    self.borrow().tdr.write(|w| w.tdr().bits(byte.into()));
                    Ok(())
                } else {
                    return Err(nb::Error::WouldBlock);
                }
            }

            fn flush(&mut self) -> nb::Result<(), Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.tc().bit_is_set() {
                    Ok(())
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }
    };
}

macro_rules! lpusart {
    ($uart:ident: {
        borrow: $borrow:ty,
        reg: ($apbXrstr:ident, $uartXrst:ident, $apbXenr:ident, $uartXen:ident, $apbX_clk:ident),
        dma: ($tx_dma_target:ident),
    }) => {
        impl<P> Serial<$uart, P>
        where
            P: Pins<$uart>,
        {
            /// Create a basic serial port
            pub fn new(uart: $uart, pins: P, baudrate: Baud, rcc: &mut Rcc) -> Self {
                rcc.rcc.$apbXrstr.modify(|_, w| w.$uartXrst().reset());
                rcc.rcc.$apbXrstr.modify(|_, w| w.$uartXrst().clear_bit());
                rcc.rcc.$apbXenr.modify(|_, w| w.$uartXen().enabled());

                uart.cr1.modify(|_, w| w.ue().disabled());

                uart.cr1.modify(|_, w| {
                    w.m0()
                        .bit8()
                        .m1()
                        .clear_bit()
                        .pce()
                        .disabled()
                        .rxneie()
                        .enabled()
                        .te()
                        .enabled()
                        .re()
                        .enabled()
                });

                uart.cr2.modify(|_, w| w.stop().stop1());

                // Set the baudrate
                // baud = 256 * Fck / brr -> brr = 256 * Fck / baud
                // brr is a 20-bit value, must be >= 0x300
                let pclk_hz = rcc.clocks.$apbX_clk();
                let brr = core::cmp::min((256 * pclk_hz.0 / baudrate.0), 0xF_FFFFu32);
                assert!(
                    brr >= 0x300,
                    "BRR value too small. Increase clock or decrease baud."
                );
                uart.brr.write(|w| w.brr().bits(brr));

                // Enable
                uart.cr1.modify(|_, w| w.ue().enabled());

                Self {
                    uart,
                    pclk_hz,
                    pins,
                }
            }

            /// Return the peripherals for reuse elsewhere
            pub fn free(self) -> ($uart, P) {
                (self.uart, self.pins)
            }

            /// Enable an interrupt
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    self.uart.cr3.modify(|_, w| w.eie().enabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    self.uart.cr1.modify(|_, w| w.peie().enabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    self.uart.cr1.modify(|_, w| w.idleie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.txeie().enabled());
                }
            }

            /// Disable an interrupt
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    self.uart.cr3.modify(|_, w| w.eie().disabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    self.uart.cr1.modify(|_, w| w.peie().disabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    self.uart.cr1.modify(|_, w| w.idleie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    self.uart.cr1.modify(|_, w| w.tcie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    self.uart.cr1.modify(|_, w| w.txeie().disabled());
                }
            }

            // /// Get the Serial port baudrate
            // pub fn baudrate(&self) -> BaudRate {
            //     let brr = self.uart.brr.read().brr().bits();
            //     BaudRate::from(4 * self.pclk_hz / (brr as u32))
            // }

            // /// Set the Serial port baudrate
            // #[inline(always)]
            // pub fn set_baudrate(&mut self, baud_rate: BaudRate) {
            //     let brr = (4 * self.pclk_hz / baud_rate.0) as u16;
            //     self.uart.brr.write(|w| w.brr().bits(brr.into()));
            // }
            /// Split the serial port into interrupt-driven Tx and Rx halves
            #[inline(always)]
            pub fn split(self) -> (Tx<$uart>, Rx<$uart>) {
                (
                    Tx {
                        _uart: core::marker::PhantomData,
                    },
                    Rx {
                        _uart: core::marker::PhantomData,
                    },
                )
            }
        }

        impl EnablePeripheral for Rx<$uart> {
            type Error = core::convert::Infallible;

            /// Enable receiver
            #[inline(always)]
            fn enable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.re().enabled());
                Ok(())
            }

            /// Return true if receiver is enabled
            #[inline(always)]
            fn is_enabled(&self) -> Result<bool, Self::Error> {
                Ok(self.borrow().cr1.read().re().is_enabled())
            }
        }

        impl DisablePeripheral for Rx<$uart> {
            /// Disable receiver
            #[inline(always)]
            fn disable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.re().disabled());
                Ok(())
            }
        }

        impl Rx<$uart> {
            /// Enable an interrupt
            ///
            /// Ignores interrupt types not related to reception
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    uart.cr3.modify(|_, w| w.eie().enabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    uart.cr1.modify(|_, w| w.peie().enabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    uart.cr1.modify(|_, w| w.idleie().enabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    uart.cr1.modify(|_, w| w.rxneie().enabled());
                }
            }

            /// Disable an interrupt
            ///
            /// Ignores interrupt types not related to reception
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.intersects(Interrupt::FRAMING | Interrupt::OVERRUN | Interrupt::NOISE)
                {
                    uart.cr3.modify(|_, w| w.eie().disabled());
                }
                if interrupt.contains(Interrupt::PARITY) {
                    uart.cr1.modify(|_, w| w.peie().disabled());
                }
                if interrupt.contains(Interrupt::IDLE) {
                    uart.cr1.modify(|_, w| w.idleie().disabled());
                }
                if interrupt.contains(Interrupt::RECEIVE_NOT_EMPTY) {
                    uart.cr1.modify(|_, w| w.rxneie().disabled());
                }
            }
        }

        impl Borrow<$borrow> for Rx<$uart> {
            fn borrow(&self) -> &$borrow {
                unsafe { &(*$uart::ptr()) }
            }
        }

        impl Read<u8> for Rx<$uart> {
            type Error = Error;

            fn read(&mut self) -> nb::Result<u8, Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.ore().bit_is_set() {
                    self.borrow().icr.write(|w| w.orecf().clear());
                    Err(nb::Error::Other(Error::Overrun))
                } else if isr.nf().bit_is_set() {
                    self.borrow().icr.write(|w| w.ncf().clear());
                    Err(nb::Error::Other(Error::Noise))
                } else if isr.fe().bit_is_set() {
                    self.borrow().icr.write(|w| w.fecf().clear());
                    Err(nb::Error::Other(Error::Framing))
                } else if isr.rxne().bit_is_set() {
                    let byte = (self.borrow().rdr.read().rdr().bits() & 0xFF) as u8;
                    Ok(byte)
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }

        impl EnablePeripheral for Tx<$uart> {
            type Error = core::convert::Infallible;

            /// Enable Transmitter
            #[inline(always)]
            fn enable(&mut self) -> Result<(), Self::Error> {
                self.borrow().cr1.modify(|_, w| w.te().enabled());
                Ok(())
            }

            /// Return true if transmitter is enabled
            #[inline(always)]
            fn is_enabled(&self) -> Result<bool, Self::Error> {
                Ok(self.borrow().cr1.read().te().is_enabled())
            }
        }

        impl DisablePeripheral for Tx<$uart> {
            #[inline(always)]
            fn disable(&mut self) -> Result<(), Self::Error> {
                let uart = self.borrow();
                // Wait for pending transmissions to clear before disabling
                while !uart.isr.read().tc().bit_is_set() {}
                uart.cr1.modify(|_, w| w.te().disabled());
                Ok(())
            }
        }

        impl Tx<$uart> {
            /// Enable an interrupt
            ///
            /// Ignores interrupts not related to transmission
            pub fn interrupt_enable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    uart.cr1.modify(|_, w| w.tcie().enabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    uart.cr1.modify(|_, w| w.txeie().enabled());
                }
            }

            /// Disable an interrupt
            ///
            /// Ignores interrupts not related to transmission
            pub fn interrupt_disable(&mut self, interrupt: Interrupt) {
                let uart = self.borrow();
                if interrupt.contains(Interrupt::TRANSMIT_COMPLETE) {
                    uart.cr1.modify(|_, w| w.tcie().disabled());
                }
                if interrupt.contains(Interrupt::TRANSMIT_EMPTY) {
                    uart.cr1.modify(|_, w| w.txeie().disabled());
                }
            }

            pub fn into_dma<Ch, const N: usize>(
                self,
                buffer: Pin<&'static mut heapless::Vec<u8, N>>,
                mut channel: Ch,
                _handle: &mut crate::dma::Dma1,
            ) -> crate::dma::serial::Tx<$uart, $tx_dma_target, Ch, N>
            where
                Ch: crate::dma::serial::UartChannel<$tx_dma_target, FromMemory>,
            {
                channel.set_memory_address(buffer.deref().as_slice().as_ptr());

                // Enable DMA for Transmit and error interrupts.
                let uart = self.borrow();

                // Wait for end of transmission
                while !uart.isr.read().tc().bit_is_set() {}

                uart.cr3.modify(|_, w| w.eie().enabled().dmat().enabled());

                crate::dma::serial::Tx {
                    state: Some(crate::dma::serial::State::Ready { channel, buffer }),
                    tx: self,
                    _target: PhantomData {},
                }
            }
        }

        impl Borrow<$borrow> for Tx<$uart> {
            fn borrow(&self) -> &$borrow {
                unsafe { &(*$uart::ptr()) }
            }
        }

        impl Write<u8> for Tx<$uart> {
            type Error = Error;

            fn write(&mut self, byte: u8) -> nb::Result<(), Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.teack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.txe().bit_is_set() {
                    self.borrow().tdr.write(|w| w.tdr().bits(byte.into()));
                    Ok(())
                } else {
                    return Err(nb::Error::WouldBlock);
                }
            }

            fn flush(&mut self) -> nb::Result<(), Self::Error> {
                let isr = self.borrow().isr.read();
                if !isr.reack().bit_is_set() {
                    Err(nb::Error::Other(Error::NotEnabled))
                } else if isr.tc().bit_is_set() {
                    Ok(())
                } else {
                    Err(nb::Error::WouldBlock)
                }
            }
        }
    };
}

uart!(USART1: {
    borrow: usart1::RegisterBlock,
    reg: (apb2rstr, usart1rst, apb2enr, usart1en, apb2_clk),
    dma: (USART1_TX),
});

uart!(USART2: {
    borrow: usart1::RegisterBlock,
    reg: (apb1rstr, usart2rst, apb1enr, usart2en, apb1_clk),
    dma: (USART2_TX),
});

lpusart!(LPUART1: {
    borrow: lpuart1::RegisterBlock,
    reg: (apb1rstr, lpuart1rst, apb1enr, lpuart1en, apb1_clk),
    dma: (LPUART1_TX),
});
