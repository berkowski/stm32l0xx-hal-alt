//! Reset and Clock Control
//!
//! Borrowed heavily from RCC hal in stm32l0xx-hal
use embedded_hal::blocking::delay::DelayMs;
use embedded_time::{duration::Milliseconds, rate::Hertz};

use crate::{
    pac::{rcc::csr::LSEDRV_A, RCC, TIM21},
    pwr::Pwr,
};

use core::marker::PhantomData;

/// High-Speed internal oscillator frequency in Hz
pub const HSI_FREQ: Hertz<u32> = Hertz(16_000_000);

bitflags::bitflags! {
    pub struct Interrupt: u32 {
        /// set by hardware when the LSI clock becomes stable
        const LSI_READY = 0b1 << 0;
        /// set by hardware when the LSE clock becomes stable
        const LSE_READY = 0b1 << 1;
        /// set by hardware when the HSI16 clock becomes stable
        const HSI16_READY = 0b1 << 2;
        /// set by hardware when the HSE clock becomes stable
        const HSE_READY = 0b1 << 3;
        /// set by hardware when the PLL clock becomes stable
        const PLL_READY = 0b1 << 4;
        /// set by hardware when the MSI clock becomes stable
        const MSI_READY = 0b1 << 5;
        /// set by hardware when the LSE security system is enabled and the LSE clock fails
        const LSE_FAILED = 0b1 << 7;
        /// set by hardware when the HSE security system is enabled and hte HSE clock fails
        const HSE_FAILED = 0b1 << 8;
    }
}

bitflags::bitflags! {
/// Reset event type
pub struct ResetEvent :u32 {
    /// Low-power management reset
    const LOW_POWER = 1 << 31;
    /// Window watchdog reset
    const WINDOW_WATCHDOG = 1 << 30;
    /// Independent watchdog reset
    const INDEPENDENT_WATCHDOG = 1 << 29;
    /// Software reset
    const SOFTWARE = 1 << 28;
    /// Power-on reset
    const POWER_ON = 1 << 27;
    /// nRST pin
    const PIN = 1 << 26;
    /// Option bytes loading reset
    const OPTION_BYTES_LOADING = 1<< 25;
    /// Firewall reset
    const FIREWALL = 1 << 24;
}
}

/// Low-speed internal clock
pub struct Lsi<T> {
    pub(self) _state: PhantomData<T>,
}

impl<T> Lsi<T> {
    pub fn disable(self, rcc: &mut Rcc) -> Lsi<Disabled> {
        rcc.rcc.csr.modify(|_, w| w.lsion().off());
        Lsi {
            _state: PhantomData {},
        }
    }
}

impl Lsi<Disabled> {
    pub fn enable(self, rcc: &mut Rcc) -> Lsi<NotReady> {
        rcc.rcc.csr.modify(|_, w| w.lsion().set_bit());
        while rcc.rcc.csr.read().lsion().bit_is_clear() {}
        Lsi {
            _state: PhantomData {},
        }
    }
}

impl Lsi<NotReady> {
    /// Check if the "ready" bit has been asserted and that the clock has stabilized
    pub fn is_ready(&self) -> nb::Result<(), core::convert::Infallible> {
        let rcc = unsafe { &*RCC::PTR };
        if rcc.csr.read().lsirdy().bit_is_clear() {
            Err(nb::Error::WouldBlock)
        } else {
            Ok(())
        }
    }

    /// Blocking wait for the clock to be ready.
    ///
    /// If there is a failure with hardware, this may block forever!
    pub fn wait(self) -> Lsi<Ready> {
        nb::block!(self.is_ready()).unwrap();
        Lsi {
            _state: PhantomData {},
        }
    }

    /// Wait for the clock to be ready, or the given timeout expires.
    ///
    /// Will check the ready bit every `poll_interval` milliseconds until the clock is ether
    /// ready or `timeout` milliseconds elapses
    pub fn wait_timeout<D: DelayMs<u32>>(
        self,
        timeout: Milliseconds<u32>,
        poll_interval: Milliseconds<u32>,
        delay: &mut D,
    ) -> Result<Lsi<Ready>, Lsi<NotReady>> {
        let mut timeout = timeout.0;
        let poll_interval = poll_interval.0;

        while timeout > 0 {
            // ready?
            if self.is_ready().is_ok() {
                return Ok(Lsi {
                    _state: PhantomData {},
                });
            }

            // wait a bit and try again
            let wait = match timeout.cmp(&poll_interval) {
                core::cmp::Ordering::Greater | core::cmp::Ordering::Equal => poll_interval,
                core::cmp::Ordering::Less => timeout,
            };
            delay.delay_ms(wait);
            timeout = timeout.saturating_sub(wait);
        }

        Err(self)
    }
}

impl Lsi<Ready> {
    /// Attempt to measure LSI frequency against system clock
    ///
    /// This is an adaptation of Application Note AN4631: "How to Calibrate an STM32L0xx Internal RC Oscillator"
    /// without using interrupts.  It should be used early on in initialization code, before other
    /// peripherals are activated and before interrupts are enabled.
    ///
    /// TIM21 is reset and disabled upon completion of the measurement
    pub fn measure_freq(
        &self,
        timer: &TIM21,
        lse: Option<&Lse<Ready>>,
        rcc: &mut Rcc,
    ) -> Hertz<u32> {
        unsafe {
            rcc.with_raw(|handle| {
                handle.apb2enr.modify(|_, w| w.tim21en().enabled());
                handle.apb2rstr.modify(|_, w| w.tim21rst().reset());
                handle.apb2rstr.modify(|_, w| w.tim21rst().clear_bit());
            })
        };

        // Remap TI1 to LSI clock
        unsafe { timer.or.modify(|_, w| w.ti1_rmp().bits(0b101)) };

        let timer_clock_freq: u32;
        let input_prescalar: u32;
        let input_prescalar_bits: u8;

        if lse.is_some() {
            // use LSE clock for external clock input
            timer.or.modify(|_, w| w.etr_rmp().bits(0b11));
            timer.smcr.modify(|_, w| w.ece().set_bit());
            // Since LSE is slower than LSI, use a prescalar /4 on input channel
            input_prescalar = 8;
            input_prescalar_bits = 0b11;
            timer_clock_freq = 32_768;
        } else {
            input_prescalar = 1;
            input_prescalar_bits = 0b00;
            timer_clock_freq = rcc.clocks.apb2_tim_clk().0;
        }

        // configure input capture with CC1 as input, IC1 mapped to TI1
        timer.ccmr1_input().modify(|_, w| w.cc1s().bits(0b01));
        timer.ccer.modify(|_, w| w.cc1e().set_bit());

        const N_LOOPS: i32 = 100;
        let mut total: i32 = 0;
        for n in 1..=N_LOOPS {
            // start timer and wait for first edge
            timer.ccer.modify(|_, w| w.cc1e().set_bit());
            timer
                .ccmr1_input()
                .modify(|_, w| w.ic1psc().bits(input_prescalar_bits));
            timer.cr1.modify(|_, w| w.cen().enabled());
            while timer.sr.read().cc1if().bit_is_clear() {}
            let t1 = timer.ccr1.read().ccr().bits() as i32;
            timer.sr.modify(|r, w| {
                if r.cc1of().bit_is_set() {
                    defmt::debug!("lsi cal loop {}: overcapture detected on t1", n);
                }
                w.cc1of().clear_bit()
            });

            while timer.sr.read().cc1if().bit_is_clear() {}
            let t2 = timer.ccr1.read().ccr().bits() as i32;
            timer.sr.modify(|r, w| {
                if r.cc1of().bit_is_set() {
                    defmt::debug!("lsi cal loop {}: overcapture detected on t2", n);
                }
                w.cc1of().clear_bit()
            });
            timer.cr1.modify(|_, w| w.cen().disabled());
            timer.ccer.modify(|_, w| w.cc1e().clear_bit());
            let cnt = (t2 - t1) * 100;
            total += cnt;
        }

        debug_assert!(total > 0);
        let avg = (total as u32) / ((100 * N_LOOPS) as u32);

        // Reset and disable TIM21
        unsafe {
            rcc.with_raw(|handle| {
                handle.apb2rstr.modify(|_, w| w.tim21rst().reset());
                handle.apb2rstr.modify(|_, w| w.tim21rst().clear_bit());
                handle.apb2enr.modify(|_, w| w.tim21en().disabled());
            })
        };

        Hertz(timer_clock_freq * input_prescalar / avg)
    }
}

/// Clock is disabled
pub struct Disabled {}
/// Clock is Ready
pub struct Ready {}
/// Clock is Enabled but not yet ready
pub struct NotReady {}

/// Low-speed External Clock
pub struct Lse<T> {
    pub(self) _state: PhantomData<T>,
}

impl<T> Lse<T> {
    /// Turn off the low-speed external clock
    pub fn disable(self, rcc: &mut Rcc, pwr: &mut Pwr) -> Lse<Disabled> {
        pwr.disable_backup_protection();
        unsafe { rcc.with_raw(|handle| handle.csr.modify(|_, w| w.lseon().off())) }
        Lse {
            _state: PhantomData,
        }
    }
}

impl Lse<Disabled> {
    /// Enable the low-speed external clock using an external crystal
    ///
    /// This only powers up the LSE clock, it does NOT wait to check if it's
    /// actually useable.  To do that you'll want to use is_ready, wait, or wait_timeout
    pub fn enable_external_crystal(
        self,
        drive: LSEDRV_A,
        rcc: &mut Rcc,
        pwr: &mut Pwr,
    ) -> Lse<NotReady> {
        pwr.disable_backup_protection();

        unsafe {
            rcc.with_raw(|handle| {
                handle
                    .csr
                    .modify(|_, w| w.lsedrv().variant(drive).lseon().set_bit());
                while handle.csr.read().lseon().bit_is_clear() {}
            })
        }

        Lse {
            _state: PhantomData {},
        }
    }

    /// Enable the low-speed external clock using an external clock source
    ///
    /// The clock source must be 32.768kHz at 50% duty cycle
    ///
    /// This only powers up the LSE clock, it does NOT wait to check if it's
    /// actually useable.  To do that you'll want to use is_ready, wait, or wait_timeout
    pub fn enable_external_source(self, rcc: &mut Rcc, pwr: &mut Pwr) -> Lse<NotReady> {
        pwr.disable_backup_protection();
        unsafe {
            rcc.with_raw(|handle| {
                handle
                    .csr
                    .modify(|_, w| w.lseon().set_bit().lsebyp().set_bit());
                while handle.csr.read().lseon().bit_is_clear() {}
            })
        }

        Lse {
            _state: PhantomData {},
        }
    }
}

impl Lse<NotReady> {
    /// Check if the "ready" bit has been asserted and that the clock has stabilized
    pub fn is_ready(&self) -> nb::Result<(), core::convert::Infallible> {
        let rcc = unsafe { &*RCC::PTR };
        if rcc.csr.read().lserdy().bit_is_clear() {
            Err(nb::Error::WouldBlock)
        } else {
            Ok(())
        }
    }

    /// Blocking wait for the clock to be ready.
    ///
    /// If there is a failure with hardware, this may block forever!
    pub fn wait(self) -> Lse<Ready> {
        nb::block!(self.is_ready()).unwrap();
        Lse {
            _state: PhantomData {},
        }
    }

    /// Wait for the clock to be ready, or the given timeout expires.
    ///
    /// Will check the ready bit every `poll_interval` milliseconds until the clock is ether
    /// ready or `timeout` milliseconds elapses
    pub fn wait_timeout<D: DelayMs<u32>>(
        self,
        timeout: Milliseconds<u32>,
        poll_interval: Milliseconds<u32>,
        delay: &mut D,
    ) -> Result<Lse<Ready>, Lse<NotReady>> {
        let mut timeout = timeout.0;
        let poll_interval = poll_interval.0;

        while timeout > 0 {
            // ready?
            if self.is_ready().is_ok() {
                return Ok(Lse {
                    _state: PhantomData {},
                });
            }

            // wait a bit and try again
            let wait = match timeout.cmp(&poll_interval) {
                core::cmp::Ordering::Greater | core::cmp::Ordering::Equal => poll_interval,
                core::cmp::Ordering::Less => timeout,
            };
            delay.delay_ms(wait);
            timeout = timeout.saturating_sub(wait);
        }

        Err(self)
    }
}

impl Lse<Ready> {
    /// Enable clock security on LSE
    ///
    /// Use with the LSE_FAILURE interrupt enabled, will fire the global RCC
    /// interrupt on a hardware failure of the LSE clock.  Requires LSE, LSI, and
    /// RTC peripherals to have been initialized.
    pub fn enable_css(&mut self, _lsi: &Lsi<Ready>, rcc: &mut Rcc) {
        // having Rtc, Lsi, and Lse<Ready> ensures that the Lse is on and ready,
        // the Lsi is on and ready, and the clock has been selected for the Rtc
        rcc.rcc.csr.modify(|_, w| w.csslseon().on());
    }
}

/// Reset configuration control structure
///
/// Available once the system clock has been configured.
pub struct Rcc {
    pub(crate) rcc: RCC,
    /// Clock frequencies of peripheral busses.
    pub clocks: PeripheralClocks,

    lsi: Option<Lsi<Disabled>>,
    lse: Option<Lse<Disabled>>,
}

impl Rcc {
    /// Return the underlying RCC peripheral
    pub fn free(self) -> RCC {
        self.rcc
    }

    /// Gain access to the underlying RCC peripherial
    ///
    /// As this HAL doesn't provide full high-level access ot all
    /// peripherals the user may need access to the RCC for their own
    /// configuration/setup needs.
    ///
    /// # Safety
    /// Modifying the RCC in this manor carries the risk of invalidating
    /// all other assumptions this HAL provides.  Users should be *extremely*
    /// cautious when manipulating the bare RCC registers.
    pub unsafe fn with_raw<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&RCC) -> R,
    {
        f(&self.rcc)
    }

    /// Get the reset event
    ///
    /// If you expect a single reset event to be set you should call
    /// reset_clear() afterwards to reset all events.
    #[inline(always)]
    pub fn reset_event(&self) -> ResetEvent {
        ResetEvent::from_bits_truncate(self.rcc.csr.read().bits())
    }

    /// Clear reset event flags
    #[inline(always)]
    pub fn reset_clear(&mut self) {
        self.rcc.csr.modify(|_, w| w.rmvf().set_bit());
    }

    /// Constrain the system clock to the provided config
    pub fn constrain(config: Config, rcc: RCC) -> Self {
        // grab the sys_clk and sw_bits
        let (sys_clk, sw_bits) = match config.src {
            ClockSource::MSI(speed) => {
                let speed = speed as u8;
                // Set MSI range
                rcc.icscr.write(|w| w.msirange().bits(speed));

                // Enable MSI
                rcc.cr.write(|w| w.msion().set_bit());
                while rcc.cr.read().msirdy().bit_is_clear() {}

                let freq: Hertz<u32> = Hertz(32_768 * (1 << (speed + 1)));
                (freq, 0)
            }
            ClockSource::HSI16 => {
                // Enable HSI16
                rcc.cr.write(|w| w.hsi16on().set_bit());
                while rcc.cr.read().hsi16rdyf().bit_is_clear() {}

                (HSI_FREQ, 1)
            } //ClockSource::HSE(freq) => {
              //    // Enable HSE
              //    rcc.cr.write(|w| w.hseon().set_bit());
              //    while self.cr.read().hserdy().bit_is_clear() {}

              //    (freq.0, 2)
              //}
              // ClockSrc::PLL(src, mul, div) => {
              //     let (src_bit, freq) = match src {
              //         PLLSource::HSE(freq) => {
              //             // Enable HSE
              //             self.cr.write(|w| w.hseon().set_bit());
              //             while self.cr.read().hserdy().bit_is_clear() {}
              //             (true, freq.0)
              //         }
              //         PLLSource::HSI16 => {
              //             // Enable HSI
              //             self.cr.write(|w| w.hsi16on().set_bit());
              //             while self.cr.read().hsi16rdyf().bit_is_clear() {}
              //             (false, HSI_FREQ)
              //         }
              //     };

              //     // Disable PLL
              //     self.cr.modify(|_, w| w.pllon().clear_bit());
              //     while self.cr.read().pllrdy().bit_is_set() {}

              //     let mul_bytes = mul as u8;
              //     let div_bytes = div as u8;

              //     let freq = match mul {
              //         PLLMul::Mul3 => freq * 3,
              //         PLLMul::Mul4 => freq * 4,
              //         PLLMul::Mul6 => freq * 6,
              //         PLLMul::Mul8 => freq * 8,
              //         PLLMul::Mul12 => freq * 12,
              //         PLLMul::Mul16 => freq * 16,
              //         PLLMul::Mul24 => freq * 24,
              //         PLLMul::Mul32 => freq * 32,
              //         PLLMul::Mul48 => freq * 48,
              //     };

              //     let freq = match div {
              //         PLLDiv::Div2 => freq / 2,
              //         PLLDiv::Div3 => freq / 3,
              //         PLLDiv::Div4 => freq / 4,
              //     };
              //     assert!(freq <= 24.mhz().0);

              //     self.cfgr.write(move |w| unsafe {
              //         w.pllmul()
              //             .bits(mul_bytes)
              //             .plldiv()
              //             .bits(div_bytes)
              //             .pllsrc()
              //             .bit(src_bit)
              //     });

              //    // Enable PLL
              //    self.cr.modify(|_, w| w.pllon().set_bit());
              //    while self.cr.read().pllrdy().bit_is_clear() {}

              //    (freq, 3)
              //}
        };

        #[rustfmt::skip]
        rcc.cfgr.modify(|_, w| unsafe {
            w.sw()
                .bits(sw_bits).hpre()
                .bits(config.ahb_pre as u8).ppre1()
                .bits(config.apb1_pre as u8).ppre2()
                .bits(config.apb2_pre as u8)
        });

        let ahb_freq = match config.ahb_pre {
            AHBPrescaler::NotDivided => sys_clk,
            pre => sys_clk / (1 << (pre as u8 - 7)),
        };

        let (apb1_freq, apb1_tim_freq) = match config.apb1_pre {
            APBPrescaler::NotDivided => (ahb_freq, ahb_freq),
            pre => {
                let freq = ahb_freq / (1 << (pre as u8 - 3));
                (freq, freq * 2)
            }
        };

        let (apb2_freq, apb2_tim_freq) = match config.apb2_pre {
            APBPrescaler::NotDivided => (ahb_freq, ahb_freq),
            pre => {
                let freq = ahb_freq / (1 << (pre as u8 - 3));
                (freq, freq * 2)
            }
        };

        let clocks = PeripheralClocks {
            source: config.src,
            sys_clk,
            ahb_clk: ahb_freq,
            apb1_clk: apb1_freq,
            apb2_clk: apb2_freq,
            apb1_tim_clk: apb1_tim_freq,
            apb2_tim_clk: apb2_tim_freq,
        };

        Self {
            rcc,
            clocks,
            lsi: Some(Lsi {
                _state: PhantomData {},
            }),
            lse: Some(Lse {
                _state: PhantomData {},
            }),
        }
    }

    /// Get access to the low-speed internal clock peripheral
    pub fn take_lsi(&mut self) -> Option<Lsi<Disabled>> {
        core::mem::replace(&mut self.lsi, None)
    }

    /// Get access to the low-speed external clock peripheral
    pub fn take_lse(&mut self) -> Option<Lse<Disabled>> {
        core::mem::replace(&mut self.lse, None)
    }

    pub fn pending(&self) -> Interrupt {
        Interrupt::from_bits_truncate(self.rcc.cifr.read().bits())
    }

    pub fn enable_interrupts(&mut self, interrupts: Interrupt) {
        // The cier register on RCC is only listed as readable from the pac,
        // AND in the datasheet, but the wording in the datasheet makes it seem
        // software is responsable for enabling interrupts:
        //
        // Bit 7 CSSLSE: LSE CSS interrupt flag
        //     This bit is set and reset by software to enable/disable the interrupt caused by the Clock
        //     Security System on external 32 kHz oscillator.
        //     0: LSE CSS interrupt disabled
        //     1: LSE CSS interrupt enabled
        unsafe {
            let cier: *mut u32 = (0x4002_1000u32 + 0x10) as *mut _;
            cier.write_volatile(cier.read_volatile() | interrupts.bits());
        }
    }

    pub fn disable_interrupts(&mut self, interrupts: Interrupt) {
        // The cier register on RCC is only listed as readable from the pac,
        // AND in the datasheet, but the wording in the datasheet makes it seem
        // software is responsable for enabling interrupts:
        //
        // Bit 7 CSSLSE: LSE CSS interrupt flag
        //     This bit is set and reset by software to enable/disable the interrupt caused by the Clock
        //     Security System on external 32 kHz oscillator.
        //     0: LSE CSS interrupt disabled
        //     1: LSE CSS interrupt enabled
        unsafe {
            let cier: *mut u32 = (0x4002_1000u32 + 0x10) as *mut _;
            cier.write_volatile(cier.read_volatile() & (!interrupts.bits()));
        }
    }

    pub fn clear_interrupts(&mut self, interrupts: Interrupt) {
        // The cicr register on RCC is only listed as readable from the pac,
        // so do this the old way.  cicr is at an offset of 0x18 from the base RCC
        // reg address
        unsafe {
            let cicr: *mut u32 = (0x4002_1000u32 + 0x18) as *mut _;
            cicr.write_volatile(interrupts.bits());
        }
    }
}

/// Clock sources available to drive the system clock
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum ClockSource {
    /// High-speed internal 16MHz oscillator clock
    HSI16,
    /// High-speed external oscillator clock
    // HSE,
    /// Phase-Locked Loop clock
    // PLL,
    /// Multi-speed internal oscillator clock,
    MSI(MSISpeed),
}

impl Default for ClockSource {
    fn default() -> Self {
        ClockSource::MSI(MSISpeed::default())
    }
}

/// Valid clock speeds for the Medium Speed Internal oscillator
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum MSISpeed {
    /// 65.536 kHz
    _65_536kHz = 0b000,
    /// 131.072kHz
    _131_072kHz = 0b001,
    /// 262.144kHz
    _262_144kHz = 0b010,
    /// 524.288kHz,
    _524_288kHz = 0b011,
    /// 1.048MHz
    _1_048MHz = 0b100,
    /// 2.097MHz,
    _2_097MHz = 0b101,
    /// 4.194MHz,
    _4_194MHz = 0b110,
}

impl Default for MSISpeed {
    fn default() -> Self {
        MSISpeed::_2_097MHz
    }
}

/// AHB prescaler
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum AHBPrescaler {
    /// Not divided (divide by 1)
    NotDivided = 0,
    /// Div by 2
    Div2 = 0b1000,
    /// Div by 4
    Div4 = 0b1001,
    /// Div by 8
    Div8 = 0b1010,
    /// Div by 16
    Div16 = 0b1011,
    /// Div by 64
    Div64 = 0b1100,
    /// Div by 128
    Div128 = 0b1101,
    /// Div by 256
    Div256 = 0b1110,
    /// Div by 512
    Div512 = 0b1111,
}

impl Default for AHBPrescaler {
    fn default() -> Self {
        Self::NotDivided
    }
}

/// APB prescaler
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum APBPrescaler {
    /// Not divided (div by 1)
    NotDivided = 0,
    /// Div by 2
    Div2 = 0b100,
    /// Div by 4
    Div4 = 0b101,
    /// Div by 8
    Div8 = 0b110,
    /// Div by 16
    Div16 = 0b111,
}

impl Default for APBPrescaler {
    fn default() -> Self {
        Self::NotDivided
    }
}

/// Intended system clock config
#[derive(Default)]
pub struct Config {
    /// System clock source
    pub src: ClockSource,
    /// High-speed peripheral clock prescaler
    pub ahb_pre: AHBPrescaler,
    /// low-speed peripheral clock 1 prescaler
    pub apb1_pre: APBPrescaler,
    /// low-speed peripheral clock 2 prescaler
    pub apb2_pre: APBPrescaler,
}

/// Frozen clock frequencies
///
/// The existence of this value indicates that the clock configuration can no longer be changed
#[derive(Clone, Copy)]
pub struct PeripheralClocks {
    source: ClockSource,
    sys_clk: Hertz<u32>,
    ahb_clk: Hertz<u32>,
    apb1_clk: Hertz<u32>,
    apb1_tim_clk: Hertz<u32>,
    apb2_clk: Hertz<u32>,
    apb2_tim_clk: Hertz<u32>,
}

impl PeripheralClocks {
    /// Returns the clock source
    pub fn source(&self) -> &ClockSource {
        &self.source
    }

    /// Returns the system (core) frequency
    pub fn sys_clk(&self) -> Hertz<u32> {
        self.sys_clk
    }

    /// Returns the frequency of the AHB
    pub fn ahb_clk(&self) -> Hertz<u32> {
        self.ahb_clk
    }

    /// Returns the frequency of the APB1
    pub fn apb1_clk(&self) -> Hertz<u32> {
        self.apb1_clk
    }

    /// Returns the frequency of the APB1 timers
    pub fn apb1_tim_clk(&self) -> Hertz<u32> {
        self.apb1_tim_clk
    }

    /// Returns the frequency of the APB2
    pub fn apb2_clk(&self) -> Hertz<u32> {
        self.apb2_clk
    }

    /// Returns the frequency of the APB2 timers
    pub fn apb2_tim_clk(&self) -> Hertz<u32> {
        self.apb2_tim_clk
    }
}
