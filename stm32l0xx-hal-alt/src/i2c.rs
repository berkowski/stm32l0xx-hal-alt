use crate::{
    gpio::{
        gpioa::{PA10, PA8, PA9},
        gpiob::{PB10, PB11, PB4, PB6, PB7, PB8, PB9},
        gpioc::{PC0, PC1, PC9},
        AF1, AF4, AF6, AF7,
    },
    pac::{
        i2c1::cr2::{AUTOEND_A, RD_WRN_A, RELOAD_A},
        I2C1, I2C2, I2C3,
    },
    rcc::Rcc,
};

use core::marker::PhantomData;
use embedded_hal::blocking::{
    delay::DelayMs,
    i2c::{SevenBitAddress, TenBitAddress},
};
use embedded_hal_ext::{DisablePeripheral, EnablePeripheral, Timeout};

pub type Error = embedded_hal_ext::i2c::blocking::ErrorKind;

macro_rules! i2c_pins {
    ($($i2c:ident: {
	sda: [$(($sda:ident, $sda_af:ident),)+],
        scl: [$(($scl:ident, $scl_af:ident),)+],
    },)+) => {

    mod pins {
	use super::*;
	pub trait Sealed {}


	$(
	    $( impl Sealed for $sda<$sda_af> {} )+
	    $( impl Sealed for $scl<$scl_af> {} )+
	)+
    }

    pub trait Sda<T> : pins::Sealed {}
    pub trait Scl<T> : pins::Sealed {}
    $(
	$( impl Sda<$i2c> for $sda<$sda_af> {} )+
	$( impl Scl<$i2c> for $scl<$scl_af> {} )+
    )+

    };
}

i2c_pins! {
    I2C1: {
      sda: [(PA10, AF6), (PB7, AF1), (PB9, AF4),],
      scl: [(PA9, AF6), (PB6, AF1), (PB8, AF4),],
    },
    I2C2: {
      sda: [(PB11, AF6),],
      scl: [(PB10, AF6),],
    },
    I2C3: {
      sda: [(PB4, AF7), (PC1, AF7), (PC9, AF7),],
      scl: [(PA8, AF7), (PC0, AF7),],
    },
}

pub struct Master;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Address {
    SevenBitAddress(SevenBitAddress),
    TenBitAddress(TenBitAddress),
}

impl TryFrom<u8> for Address {
    type Error = Error;
    fn try_from(addr: u8) -> Result<Self, Self::Error> {
        if addr > 127 {
            Err(Error::InvalidAddress)
        } else {
            Ok(Self::SevenBitAddress(addr))
        }
    }
}

impl TryFrom<u16> for Address {
    type Error = Error;
    fn try_from(addr: u16) -> Result<Self, Self::Error> {
        if addr > 1023 {
            Err(Error::InvalidAddress)
        } else {
            Ok(Self::TenBitAddress(addr))
        }
    }
}

pub struct I2c<T, D, C, M> {
    pub(crate) i2c: T,
    _sda: D,
    _scl: C,
    _mode: PhantomData<M>,
}

macro_rules! i2c {
    ($i2c:ident: {
	reg: {apb_reset: $apbXrstr:ident, apb_enable: $apbXenr:ident, apb_clock: $apbXclk:ident, i2c_reset: $i2cXrst:ident, i2c_enable: $i2cXen:ident},
    }) => {
        impl<D, C> embedded_hal_ext::i2c::Reset for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            /// Perform a software reset.
            ///
            /// Internal hardware state machines are reset.
            /// Control and status bits are return to their reset values.
            ///
            /// Configuration remains unchanged.
            fn reset(&mut self) {
                self.i2c.cr1.modify(|_, w| w.pe().disabled());
                while self.i2c.cr1.read().pe().is_enabled() {}
                self.i2c.cr1.modify(|_, w| w.pe().enabled());
            }
        }

        impl<D, C> embedded_hal_ext::i2c::blocking::ErrorType for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            type Error = embedded_hal_ext::i2c::blocking::ErrorKind;
        }

        impl<D, C>
            embedded_hal_ext::i2c::blocking::WriteRead<
                embedded_hal_ext::i2c::blocking::SevenBitAddress,
            > for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            fn write_read_with_timeout<'a, T>(
                &mut self,
                address: embedded_hal_ext::i2c::blocking::SevenBitAddress,
                bytes: &[u8],
                buffer: &mut [u8],
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<(), Self::Error>
            where
                T: DelayMs<u32>,
            {
                let address = Address::try_from(address)?;
                if buffer.len() > 0 {
                    self.write_transaction_with_timeout(address, bytes, timeout, false)?;
                    self.read_transaction_with_timeout(address, buffer, timeout, true)
                } else {
                    self.write_transaction_with_timeout(address, bytes, timeout, true)
                }
            }
        }

        impl<D, C>
            embedded_hal_ext::i2c::blocking::WriteRead<
                embedded_hal_ext::i2c::blocking::TenBitAddress,
            > for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            fn write_read_with_timeout<'a, T>(
                &mut self,
                address: embedded_hal_ext::i2c::blocking::TenBitAddress,
                bytes: &[u8],
                buffer: &mut [u8],
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<(), Self::Error>
            where
                T: DelayMs<u32>,
            {
                let address = Address::try_from(address)?;
                if buffer.len() > 0 {
                    self.write_transaction_with_timeout(address, bytes, timeout, false)?;
                    self.read_transaction_with_timeout(address, buffer, timeout, true)
                } else {
                    self.write_transaction_with_timeout(address, bytes, timeout, true)
                }
            }
        }

        impl<D, C> I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            /// Create a new I2C master-mode interface
            pub fn constrain(i2c: $i2c, scl: C, sda: D, rcc: &mut Rcc) -> Self {
                unsafe {
                    rcc.with_raw(|handle| {
                        handle.$apbXrstr.modify(|_, w| w.$i2cXrst().reset());
                        handle.$apbXrstr.modify(|_, w| w.$i2cXrst().clear_bit());
                        handle.$apbXenr.modify(|_, w| w.$i2cXen().enabled());
                    });
                }

                // Set speed, prescalars, etc.

                // ** START FROM stm32l0xx-hal
                // TODO review compliance with the timing requirements of I2C
                // t_I2CCLK = 1 / PCLK1
                // t_PRESC  = (PRESC + 1) * t_I2CCLK
                // t_SCLL   = (SCLL + 1) * t_PRESC
                // t_SCLH   = (SCLH + 1) * t_PRESC
                //
                // t_SYNC1 + t_SYNC2 > 4 * t_I2CCLK
                // t_SCL ~= t_SYNC1 + t_SYNC2 + t_SCLL + t_SCLH

                let freq = 100_000;
                let i2cclk = rcc.clocks.$apbXclk().0;
                let ratio = i2cclk / freq - 4;
                let (presc, scll, sclh, sdadel, scldel) = if freq >= 100_000 {
                    // fast-mode or fast-mode plus
                    // here we pick SCLL + 1 = 2 * (SCLH + 1)
                    let presc = ratio / 387;

                    let sclh = ((ratio / (presc + 1)) - 3) / 3;
                    let scll = 2 * (sclh + 1) - 1;

                    let (sdadel, scldel) = if freq > 400_000 {
                        // fast-mode plus
                        let sdadel = 0;
                        let scldel = i2cclk / 4_000_000 / (presc + 1) - 1;

                        (sdadel, scldel)
                    } else {
                        // fast-mode
                        let sdadel = i2cclk / 8_000_000 / (presc + 1);
                        let scldel = i2cclk / 2_000_000 / (presc + 1) - 1;

                        (sdadel, scldel)
                    };

                    (presc, scll, sclh, sdadel, scldel)
                } else {
                    // standard-mode
                    // here we pick SCLL = SCLH
                    let presc = ratio / 514;

                    let sclh = ((ratio / (presc + 1)) - 2) / 2;
                    let scll = sclh;

                    let sdadel = i2cclk / 2_000_000 / (presc + 1);
                    let scldel = i2cclk / 800_000 / (presc + 1) - 1;

                    (presc, scll, sclh, sdadel, scldel)
                };

                assert!(presc < 16);
                let presc = presc as u8;
                assert!(scldel < 16);
                let scldel = scldel as u8;
                assert!(sdadel < 16);
                let sdadel = sdadel as u8;

                assert!(sclh <= u8::MAX.into());
                let sclh = sclh as u8;

                assert!(scll <= u8::MAX.into());
                let scll = scll as u8;

                // Configure for "fast mode" (400 KHz)
                i2c.timingr.write(|w| {
                    w.presc().bits(presc);
                    w.scll().bits(scll);
                    w.sclh().bits(sclh);
                    w.sdadel().bits(sdadel);
                    w.scldel().bits(scldel)
                });

                i2c.cr1.write(|w| w.pe().set_bit());

                // ** END FROM stm32l0xx-hal
                Self {
                    i2c,
                    _scl: scl,
                    _sda: sda,
                    _mode: PhantomData {},
                }
            }

            fn read_byte(&mut self) -> nb::Result<u8, Error> {
                self.check_errors()?;
                if self.i2c.isr.read().rxne().bit_is_clear() {
                    Err(nb::Error::WouldBlock)
                } else {
                    Ok(self.i2c.rxdr.read().rxdata().bits())
                }
            }

            fn read_with_timeout<'a, T>(
                &mut self,
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<u8, Error>
            where
                T: DelayMs<u32>,
            {
                loop {
                    match self.read_byte() {
                        Err(nb::Error::Other(e)) => return Err(e),
                        Err(nb::Error::WouldBlock) => {}
                        Ok(byte) => return Ok(byte),
                    }

                    if let Some(ref mut t) = timeout {
                        (*t).spin()?;
                    }
                }
            }

            fn write_byte(&mut self, byte: u8) -> nb::Result<(), Error> {
                self.check_errors()?;

                if self.i2c.isr.read().txe().bit_is_clear() {
                    Err(nb::Error::WouldBlock)
                } else {
                    self.i2c.txdr.write(|w| w.txdata().bits(byte));
                    Ok(())
                }
            }

            fn write_with_timeout<'a, T>(
                &mut self,
                byte: u8,
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<(), Error>
            where
                T: DelayMs<u32>,
            {
                loop {
                    match self.write_byte(byte) {
                        Err(nb::Error::Other(e)) => return Err(e),
                        Err(nb::Error::WouldBlock) => {}
                        Ok(()) => return Ok(()),
                    }

                    if let Some(ref mut t) = timeout {
                        (*t).spin()?;
                    }
                }
            }

            fn check_errors(&mut self) -> Result<(), Error> {
                let isr = self.i2c.isr.read();

                if isr.berr().bit_is_set() {
                    self.i2c.icr.write(|w| w.berrcf().set_bit());
                    Err(Error::Bus)
                } else if isr.arlo().bit_is_set() {
                    self.i2c.icr.write(|w| w.arlocf().set_bit());
                    Err(Error::ArbitrationLost)
                } else if isr.nackf().bit_is_set() {
                    self.i2c.icr.write(|w| w.nackcf().set_bit());
                    Err(Error::Nack)
                } else {
                    Ok(())
                }
            }

            fn start_transaction<'a, T>(
                &mut self,
                address: Address,
                length: u8,
                direction: RD_WRN_A,
                autoend: AUTOEND_A,
                reload: RELOAD_A,
                timeout: &mut Option<Timeout<'a, T>>,
            ) -> Result<(), Error>
            where
                T: DelayMs<u32>,
            {
                // Wait for incoming/outgoing data to clear the hardware
                self.i2c.isr.write(|w| w.txe().set_bit());
                while self.i2c.isr.read().rxne().bit_is_set() {
                    self.check_errors()?;
                    if let Some(ref mut t) = timeout {
                        (*t).spin()?;
                    }
                }

                self.check_errors()?;

                self.i2c.cr2.write(|w| {
                    w.nbytes()
                        .bits(length)
                        .start()
                        .set_bit()
                        .rd_wrn()
                        .variant(direction)
                        .autoend()
                        .variant(autoend)
                        .reload()
                        .variant(reload);

                    match address {
                        Address::SevenBitAddress(addr) => {
                            w.sadd().bits(u16::from(addr) << 1).add10().bit7()
                        }
                        Address::TenBitAddress(addr) => w.sadd().bits(addr.into()).add10().bit10(),
                    }
                });

                // Wait for busy to clear
                // while self.i2c.isr.read().busy().bit_is_set(){}
                defmt::trace! {"started transaction: {} bytes", length};
                Ok(())
            }

            fn read_transaction_with_timeout<'a, T>(
                &mut self,
                address: Address,
                buffer: &mut [u8],
                timeout: &mut Option<Timeout<'a, T>>,
                finalize: bool,
            ) -> Result<(), Error>
            where
                T: DelayMs<u32>,
            {
                #[allow(non_snake_case)]
                let mut N = buffer.len();
                let mut index = 0;
                let mut first = true;

                defmt::trace! {"starting i2c read of {} bytes", N};

                loop {
                    let (size, reload, autoend) = if N > 0xFF {
                        defmt::trace! {"reload: notcomp, autoend: soft"};
                        (0xFF, RELOAD_A::NotCompleted, AUTOEND_A::Software)
                    } else if !finalize {
                        defmt::trace! {"reload: comp, autoend: soft"};
                        (N as u8, RELOAD_A::Completed, AUTOEND_A::Software)
                    } else {
                        defmt::trace! {"reload: comp, autoend: auto"};
                        (N as u8, RELOAD_A::Completed, AUTOEND_A::Automatic)
                    };

                    N -= size as usize;

                    if first {
                        self.start_transaction(
                            address,
                            size,
                            RD_WRN_A::Read,
                            autoend,
                            reload,
                            timeout,
                        )?;
                        first = false;
                    } else {
                        self.i2c.cr2.write(|w| {
                            w.nbytes()
                                .bits(size)
                                .autoend()
                                .variant(autoend)
                                .reload()
                                .variant(reload)
                                .start()
                                .set_bit()
                        });
                    }

                    for _ in 0..size {
                        buffer[index] = self.read_with_timeout(timeout)?;
                        index += 1;
                    }
                    defmt::trace! {"finished reading {} bytes", size};

                    if autoend == AUTOEND_A::Automatic {
                        defmt::trace! {"i2c read complete"};
                        return Ok(());
                    }

                    while self.i2c.isr.read().tc().is_not_complete() {
                        self.check_errors()?;
                        if let Some(t) = timeout {
                            (*t).spin()?;
                        }
                    }

                    if reload == RELOAD_A::Completed {
                        defmt::trace! {"i2c read complete"};
                        return Ok(());
                    }

                    while self.i2c.isr.read().tcr().is_not_complete() {
                        self.check_errors()?;
                        if let Some(t) = timeout {
                            (*t).spin()?;
                        }
                    }
                }
            }

            fn write_transaction_with_timeout<'a, T>(
                &mut self,
                address: Address,
                bytes: &[u8],
                timeout: &mut Option<Timeout<'a, T>>,
                finalize: bool,
            ) -> Result<(), Error>
            where
                T: DelayMs<u32>,
            {
                #[allow(non_snake_case)]
                let mut N = bytes.len();
                let mut index = 0;
                let mut first = true;

                defmt::trace! {"starting i2c write of {} bytes", N};
                loop {
                    let (size, reload, autoend) = if N > 0xFF {
                        defmt::trace! {"reload: notcomp, autoend: soft"};
                        (0xFF, RELOAD_A::NotCompleted, AUTOEND_A::Software)
                    } else if !finalize {
                        defmt::trace! {"reload: comp, autoend: soft"};
                        (N as u8, RELOAD_A::Completed, AUTOEND_A::Software)
                    } else {
                        defmt::trace! {"reload: comp, autoend: auto"};
                        (N as u8, RELOAD_A::Completed, AUTOEND_A::Automatic)
                    };

                    N -= size as usize;

                    if first {
                        self.start_transaction(
                            address,
                            size,
                            RD_WRN_A::Write,
                            autoend,
                            reload,
                            timeout,
                        )?;
                        first = false;
                    } else {
                        self.i2c.cr2.write(|w| {
                            w.nbytes()
                                .bits(size)
                                .autoend()
                                .variant(autoend)
                                .reload()
                                .variant(reload)
                                .start()
                                .set_bit()
                        });
                    }

                    for _ in 0..size {
                        self.write_with_timeout(bytes[index], timeout)?;
                        self.check_errors()?;
                        // Wait for transmission to finish
                        // while self.i2c.isr.read().txis().bit_is_clear() {
                        //     self.check_errors()?;
                        // }
                        index += 1;
                    }

                    defmt::trace! {"finished sending {} bytes", size};
                    if autoend == AUTOEND_A::Automatic {
                        defmt::trace! {"i2c write complete"};
                        return Ok(());
                    }
                    while self.i2c.isr.read().tc().is_not_complete() {
                        self.check_errors()?;
                        if let Some(t) = timeout {
                            (*t).spin()?;
                        }
                    }

                    if reload == RELOAD_A::Completed {
                        defmt::trace! {"i2c write complete"};
                        return Ok(());
                    }

                    while self.i2c.isr.read().tcr().is_not_complete() {
                        self.check_errors()?;
                        if let Some(t) = timeout {
                            (*t).spin()?;
                        }
                    }
                }
            }

            #[cfg(never)]
            fn blocking_write_iter<B>(
                &mut self,
                address: Address,
                bytes: B,
                finalize: bool,
            ) -> Result<(), Error>
            where
                B: IntoIterator<Item = u8>,
            {
                nb::block!(self.start_transaction(
                    address,
                    0xFF,
                    RD_WRN_A::WRITE,
                    AUTOEND_A::Software,
                    RELOAD_A::NotCompleted,
                ))?;

                let mut iter = bytes.into_iter();

                loop {
                    for _ in 0..0xFF {
                        if let Some(byte) = iter.next() {
                            nb::block!(self.write_byte(byte))?;
                        } else {
                            if finalize {
                                self.i2c.cr2.write(|w| w.stop().set_bit());
                            }
                            return Ok(());
                        }
                    }
                    self.i2c.cr2.write(|w| {
                        w.nbytes()
                            .bits(0xFF)
                            .autoend()
                            .variant(AUTOEND_A::Software)
                            .reload()
                            .variant(RELOAD_A::NotCompleted)
                            .start()
                            .set_bit()
                    });
                }
            }
        }

        impl<D, C> EnablePeripheral for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            type Error = core::convert::Infallible;

            fn enable(&mut self) -> Result<(), Self::Error> {
                self.i2c.cr1.modify(|_, w| w.pe().enabled());
                Ok(())
            }

            fn is_enabled(&self) -> Result<bool, Self::Error> {
                Ok(self.i2c.cr1.read().pe().is_enabled())
            }
        }

        impl<D, C> DisablePeripheral for I2c<$i2c, D, C, Master>
        where
            D: Sda<$i2c>,
            C: Scl<$i2c>,
        {
            fn disable(&mut self) -> Result<(), Self::Error> {
                self.i2c.cr1.modify(|_, w| w.pe().disabled());
                Ok(())
            }
        }
    };
}

i2c! {
    I2C1: {
    reg: {apb_reset: apb1rstr, apb_enable: apb1enr, apb_clock: apb1_clk, i2c_reset: i2c1rst, i2c_enable: i2c1en},
    }
}
