//! Flash memory interface

use crate::{pac, rcc::Rcc};
//use core::ptr;

/// EEPROM Base address
pub const EEPROM_BASE_ADDRESS: u32 = 0x0808_0000;
/// EEPROM size (in bytes)
pub const EEPROM_SIZE: u32 = 6 * 1024;

bitflags::bitflags! {
/// ADC interrupt events
pub struct Error: u32 {
    /// Write/Erase op aborted for mem controller to perform a fetch
    /// Per docs, not a real error, but used to inform that the write/erase op
    /// did not execute
    const FETCH_WHILE_WRITE_ERASE = 1 << 17;
    /// Attempt to overwrite non-zero memory
    const NOT_ZERO = 1 << 16;
    /// Attempt to read protected address
    const READ_PROT = 1 << 13;
    /// Mismatch during option byte loading between written value and loaded value
    const OPTION_INVALID = 1 << 11;
    /// Size of data to program is incorrect
    const SIZE = 1 << 10;
    /// Alignment error.  First word of a half-page is not aligned to a half-page,
    /// or one of the following words in a half-page operation does not belong to the
    /// same half-page
    const PAGE = 1 << 9;
    /// Attempt to erase write-protected address
    const WRITE_PROT = 1 << 8;
}
}

/// Flash memory interface
pub struct Flash {
    #[allow(dead_code)]
    flash: pac::FLASH,
}

impl Flash {
    /// Create the memory interface
    pub fn constrain(flash: pac::FLASH, rcc: &mut Rcc) -> Self {
        rcc.rcc.ahbrstr.modify(|_, w| w.mifrst().set_bit());
        rcc.rcc.ahbrstr.modify(|_, w| w.mifrst().clear_bit());
        rcc.rcc.ahbenr.modify(|_, w| w.mifen().enabled());

        Self { flash }
    }

    #[allow(dead_code)]
    fn with_unlocked_eeprom<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut Self) -> R,
    {
        // Unlock EEPROM and FLASH_PECR register
        if self.flash.pecr.read().pelock().bit_is_set() {
            self.flash.pekeyr.write(|w| w.pekeyr().bits(0x89AB_CDEF));
            self.flash.pekeyr.write(|w| w.pekeyr().bits(0x0203_0405));
        }
        while self.flash.pecr.read().pelock().bit_is_set() {}
        let result = f(self);
        // Lock PECR register
        self.flash.pecr.modify(|_, w| w.pelock().set_bit());
        result
    }

    #[allow(dead_code)]
    fn with_unlocked_options<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut Self) -> R,
    {
        self.with_unlocked_eeprom(|self_| {
            // Unlock option register
            self_.flash.optkeyr.write(|w| w.optkeyr().bits(0xFBEA_D9C8));
            self_.flash.optkeyr.write(|w| w.optkeyr().bits(0x2425_2627));
            f(self_)
        })
    }

    // pub fn disable_read_protection(&mut self) -> Result<(), Error> {
    //     self.with_unlocked_options(|handle| {
    //         while handle.flash.sr.read().bsy().bit_is_set() {}
    //         handle.flash.pecr.write(|w| w.erase().erase());
    //         while handle.flash.sr.read().bsy().bit_is_set() {}
    //         const OPT: u32 = 0xAAu32;
    //         let addr: *mut u32 = &mut 0x1FF8_0000u32;
    //         unsafe {addr.write_volatile((!OPT) << 16 | OPT)};
    //         while handle.flash.sr.read().bsy().bit_is_set() {}
    //         match handle.check_error_flags() {
    //             Some(e) => Err(e),
    //             None => Ok(()),
    //         }?;
    //         handle.flash.pecr.write(|w| w.obl_launch().reload());
    //         Ok(())
    //     })
    // }

    /// Check status bits for errors
    ///
    /// Clears all status bits after reading.
    ///
    /// Returns Err(Error) if any status flags indicated an error occurred,
    /// otherwise returns Ok(())
    ///
    /// As it does clear status flags this method should take &mut self, but then
    /// it couldn't be used by read_eeprom()
    fn check_error_flags(&self) -> Result<(), Error> {
        let flags = Error::from_bits_truncate(self.flash.sr.read().bits());

        if flags.is_empty() {
            Ok(())
        } else {
            self.flash.sr.write(|w| {
                w.wrperr()
                    .clear()
                    .pgaerr()
                    .clear()
                    .sizerr()
                    .clear()
                    .optverr()
                    .clear()
                    .rderr()
                    .clear()
                    .notzeroerr()
                    .clear()
                    .fwwerr()
                    .clear()
            });
            Err(flags)
        }
    }

    /// Write data to eeprom
    ///
    /// EEPROM data must be aligned to single word (4 byte) boundries
    pub fn write_eeprom(&mut self, address: *mut u32, data: &[u8]) -> Result<(), Error> {
        // Pack bytes into u32 words
        let write_bytes =
            |self_: &mut Self, address: &mut *mut u32, bytes: &[u8]| -> Result<(), Error> {
                for chunk in bytes.chunks(4) {
                    let value = chunk
                        .iter()
                        .zip((0..).step_by(8))
                        .fold(0u32, |accum, (byte, shift)| {
                            accum + ((*byte as u32) << shift)
                        });
                    while self_.flash.sr.read().bsy().bit_is_set() {}
                    unsafe {
                        address.write_volatile(value);
                        *address = address.add(1);
                    }
                    while self_.flash.sr.read().bsy().bit_is_set() {}
                    self_.check_error_flags()?;
                }
                Ok(())
            };

        // Align bytes into u32 words
        let (prefix_u8, aligned_u32, suffix_u8) = unsafe { data.align_to::<u32>() };
        let mut address = address;

        // Unlock EEPROM, then write data
        self.with_unlocked_eeprom(|self_| {
            let _ = self_.check_error_flags();
            // self_.flash.pecr.modify(|_, w| w.data().selected());
            write_bytes(self_, &mut address, prefix_u8)?;
            for word in aligned_u32 {
                while self_.flash.sr.read().bsy().bit_is_set() {}
                unsafe {
                    address.write_volatile(*word);
                    address = address.add(1);
                }
                while self_.flash.sr.read().bsy().bit_is_set() {}
                self_.check_error_flags()?
            }
            write_bytes(self_, &mut address, suffix_u8)
        })
    }

    /// Read data from EEPROM
    ///
    /// EEPROM data must be aligned to single word (4 byte) boundries
    pub fn read_eeprom(&self, address: *const u32, data: &mut [u8]) -> Result<(), Error> {
        // Pack bytes into u32 words
        let read_bytes = |address: &mut *const u32, bytes: &mut [u8]| {
            for chunk in bytes.chunks_mut(4) {
                while self.flash.sr.read().bsy().bit_is_set() {}
                let value = unsafe { address.read_volatile() };
                for (byte, shift) in chunk.iter_mut().zip((0..).step_by(8)) {
                    *byte = ((value >> shift) & 0xFF) as u8;
                }
                while self.flash.sr.read().bsy().bit_is_set() {}
                self.check_error_flags()?;
                unsafe { *address = address.add(1) };
            }
            Ok(())
        };

        // Align bytes into u32 words
        let (prefix_u8, aligned_u32, suffix_u8) = unsafe { data.align_to_mut::<u32>() };
        let mut address = address;

        read_bytes(&mut address, &mut *prefix_u8)?;
        for word in &mut aligned_u32[..] {
            while self.flash.sr.read().bsy().bit_is_set() {}
            unsafe {
                *word = address.read_volatile();
                address = address.add(1);
            }
            self.check_error_flags()?;
        }
        read_bytes(&mut address, &mut *suffix_u8)
    }
}
