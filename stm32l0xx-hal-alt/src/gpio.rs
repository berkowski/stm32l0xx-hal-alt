//! General Purpose Input / Output
//! Minor adaptions from stm32l0xx-hal crate: <https://github.com/stm32-rs/stm32l0xx-hal/blob/master/src/gpio.rs>
//! Primarily allows "double-downgrade" to collect pins from different ports into a collection.
use core::marker::PhantomData;

use crate::{pac, rcc::Rcc};
use embedded_hal::digital::v2::{toggleable, InputPin, OutputPin, StatefulOutputPin};

/// Input mode (type state)
pub struct Input<MODE> {
    _mode: PhantomData<MODE>,
}

/// Floating input (type state)
pub struct Floating;

/// Pulled down input (type state)
pub struct PullDown;

/// Pulled up input (type state)
pub struct PullUp;

/// Open drain input or output (type state)
pub struct OpenDrain;

/// Analog mode (type state)
pub struct Analog;

mod private {
    pub trait Sealed {}
    impl Sealed for super::AF0 {}
    impl Sealed for super::AF1 {}
    impl Sealed for super::AF2 {}
    impl Sealed for super::AF3 {}
    impl Sealed for super::AF4 {}
    impl Sealed for super::AF5 {}
    impl Sealed for super::AF6 {}
    impl Sealed for super::AF7 {}
}

/// Sealed trait to obtain numeric value of alternate function mode.  Not to be
/// implemented outside of this crate.
pub trait AltMode: private::Sealed {
    /// The numerical AF mode index.
    ///
    /// For example, for AF0 types, mode() returns 0.  For AF4 mode() returns 4, etc.
    fn mode() -> u32;
}

macro_rules! af_mode {
    ($AF:ident, $i:expr, $doc:expr) => {
        #[doc=$doc]
        pub struct $AF;

        impl AltMode for $AF {
            #[inline(always)]
            fn mode() -> u32 {
                $i
            }
        }
    };
}

af_mode!(AF0, 0, "Alternate Function Mode 0 (type state)");
af_mode!(AF1, 1, "Alternate Function Mode 1 (type state)");
af_mode!(AF2, 2, "Alternate Function Mode 2 (type state)");
af_mode!(AF3, 3, "Alternate Function Mode 3 (type state)");
af_mode!(AF4, 4, "Alternate Function Mode 4 (type state)");
af_mode!(AF5, 5, "Alternate Function Mode 5 (type state)");
af_mode!(AF6, 6, "Alternate Function Mode 6 (type state)");
af_mode!(AF7, 7, "Alternate Function Mode 7 (type state)");

/// Output mode (type state)
pub struct Output<MODE> {
    _mode: PhantomData<MODE>,
}

/// Push pull output (type state)
pub struct PushPull;

/// GPIO Pin speed selection
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Speed {
    /// Low speed
    Low = 0,
    /// Medium speed
    Medium = 1,
    /// High speed
    High = 2,
    /// Very high speed
    VeryHigh = 3,
}

/// GPIO Port bank
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Port {
    /// Port A
    PA,
    /// Port B
    PB,
    /// Port C
    PC,
    /// Port D
    PD,
    /// Port E
    PE,
    /// Port H
    PH,
}

/// Fully erased GPIO pin
///
/// Allows collection of pins across multiple ports with the same mode
/// at the expense of run-time overhead.
pub struct Pin<MODE> {
    port: Port,
    i: u8,
    _mode: PhantomData<MODE>,
}

impl<MODE> Pin<MODE> {
    /// Returns the port this pin is part of.
    pub fn port(&self) -> Port {
        self.port
    }

    /// Returns this pin's number inside its port.
    pub fn pin_number(&self) -> u8 {
        self.i
    }
}

impl<MODE> OutputPin for Pin<Output<MODE>> {
    type Error = ();

    fn set_high(&mut self) -> Result<(), Self::Error> {
        // NOTE(unsafe) atomic write to a stateless register
        let offset = self.i;
        unsafe {
            match self.port {
                Port::PA => {
                    (*pac::GPIOA::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PB => {
                    (*pac::GPIOB::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PC => {
                    (*pac::GPIOC::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PD => {
                    (*pac::GPIOD::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PE => {
                    (*pac::GPIOE::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PH => {
                    (*pac::GPIOH::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
            }
        }
        Ok(())
    }

    fn set_low(&mut self) -> Result<(), Self::Error> {
        // NOTE(unsafe) atomic write to a stateless register
        let offset = self.i + 16;
        unsafe {
            match self.port {
                Port::PA => {
                    (*pac::GPIOA::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PB => {
                    (*pac::GPIOB::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PC => {
                    (*pac::GPIOC::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PD => {
                    (*pac::GPIOD::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PE => {
                    (*pac::GPIOE::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
                Port::PH => {
                    (*pac::GPIOH::ptr()).bsrr.write(|w| w.bits(1 << offset));
                }
            }
        }
        Ok(())
    }
}

impl<MODE> StatefulOutputPin for Pin<Output<MODE>> {
    fn is_set_high(&self) -> Result<bool, Self::Error> {
        let is_high = self.is_set_low()?;
        Ok(is_high)
    }

    fn is_set_low(&self) -> Result<bool, Self::Error> {
        // NOTE(unsafe) atomic read with no side effects
        let is_low = unsafe {
            let offset = self.i;
            match self.port {
                Port::PA => (*pac::GPIOA::ptr()).odr.read().bits() & (1 << offset) == 0,
                Port::PB => (*pac::GPIOB::ptr()).odr.read().bits() & (1 << offset) == 0,
                Port::PC => (*pac::GPIOC::ptr()).odr.read().bits() & (1 << offset) == 0,
                Port::PD => (*pac::GPIOD::ptr()).odr.read().bits() & (1 << offset) == 0,
                Port::PE => (*pac::GPIOE::ptr()).odr.read().bits() & (1 << offset) == 0,
                Port::PH => (*pac::GPIOH::ptr()).odr.read().bits() & (1 << offset) == 0,
            }
        };
        Ok(is_low)
    }
}

impl<MODE> toggleable::Default for Pin<Output<MODE>> {}

impl<MODE> InputPin for Pin<Output<MODE>> {
    type Error = ();

    fn is_high(&self) -> Result<bool, Self::Error> {
        let is_high = !self.is_low()?;
        Ok(is_high)
    }

    fn is_low(&self) -> Result<bool, Self::Error> {
        // NOTE(unsafe) atomic read with no side effects
        // NOTE(unsafe) atomic read with no side effects
        let is_low = unsafe {
            let offset = self.i;
            match self.port {
                Port::PA => (*pac::GPIOA::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PB => (*pac::GPIOB::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PC => (*pac::GPIOC::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PD => (*pac::GPIOD::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PE => (*pac::GPIOE::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PH => (*pac::GPIOH::ptr()).idr.read().bits() & (1 << offset) == 0,
            }
        };
        Ok(is_low)
    }
}

impl<MODE> InputPin for Pin<Input<MODE>> {
    type Error = ();

    fn is_high(&self) -> Result<bool, Self::Error> {
        let is_high = !self.is_low()?;
        Ok(is_high)
    }

    fn is_low(&self) -> Result<bool, Self::Error> {
        // NOTE(unsafe) atomic read with no side effects
        let is_low = unsafe {
            let offset = self.i;
            match self.port {
                Port::PA => (*pac::GPIOA::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PB => (*pac::GPIOB::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PC => (*pac::GPIOC::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PD => (*pac::GPIOD::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PE => (*pac::GPIOE::ptr()).idr.read().bits() & (1 << offset) == 0,
                Port::PH => (*pac::GPIOH::ptr()).idr.read().bits() & (1 << offset) == 0,
            }
        };
        Ok(is_low)
    }
}

macro_rules! gpio {
    ($GPIOX:ident, $gpiox:ident, $iopxenr:ident, $PXx:ident, [
        $($PXi:ident: ($pxi:ident, $i:expr, $MODE:ty),)+
    ]) => {
        /// GPIO
        pub mod $gpiox {
            use super::Pin;
            use core::marker::PhantomData;

            use embedded_hal::digital::v2::{toggleable, InputPin, OutputPin, StatefulOutputPin};
            use super::{
                Floating, Input, OpenDrain, Output, Speed,
                PullDown, PullUp, PushPull, Analog, Port, AltMode,
                pac,
                Rcc
            };

            mod private {
                pub trait Sealed {}

                impl<MODE> Sealed for super::$PXx<MODE> {}
                $(
                    impl<MODE> Sealed for super::$PXi<MODE> {}
                )+
            }

            /// GPIO parts
            pub struct $GPIOX {
                $(
                    /// GPIO Pin
                    pub $pxi: $PXi<$MODE>,
                )+
            }

            impl $GPIOX {
                /// Enable the GPIO peripheral
                pub fn constrain(_: pac::$GPIOX, rcc: &mut Rcc) -> Self {
                    rcc.rcc.iopenr.modify(|_, w| w.$iopxenr().enabled());

                    Self {
                        $(
                            $pxi: $PXi {
                                _mode: PhantomData,
                            },
                        )+
                    }
                }
            }

            /// Partially erased pin
            pub struct $PXx<MODE> {
                i: u8,
                _mode: PhantomData<MODE>,
            }

            impl<MODE> $PXx<MODE> {
                /// The port this pin is part of.
                pub const PORT: Port = Port::$PXx;

                /// Returns the port this pin is part of.
                pub fn port(&self) -> Port {
                    Port::$PXx
                }

                /// Returns this pin's number inside its port.
                pub fn pin_number(&self) -> u8 {
                    self.i
                }

                /// Create a fully-erased pin
                pub fn downgrade(self) -> Pin<MODE> {
                    Pin {
                        port: Self::PORT,
                        i: self.i,
                        _mode: self._mode,
                    }
                }
            }

            impl<MODE> OutputPin for $PXx<Output<MODE>> {
                type Error = ();

                fn set_high(&mut self) -> Result<(), Self::Error> {
                    // NOTE(unsafe) atomic write to a stateless register
                    unsafe { (*pac::$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << self.i)) };
                    Ok(())
                }

                fn set_low(&mut self) -> Result<(), Self::Error> {
                    // NOTE(unsafe) atomic write to a stateless register
                    unsafe { (*pac::$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << (self.i + 16))) };
                    Ok(())
                }
            }

            impl<MODE> StatefulOutputPin for $PXx<Output<MODE>> {
                fn is_set_high(&self) -> Result<bool, Self::Error> {
                    let is_high = self.is_set_low()?;
                    Ok(is_high)
                }

                fn is_set_low(&self) -> Result<bool, Self::Error> {
                    // NOTE(unsafe) atomic read with no side effects
                    let is_low = unsafe { (*pac::$GPIOX::ptr()).odr.read().bits() & (1 << self.i) == 0 };
                    Ok(is_low)
                }
            }

            impl<MODE> toggleable::Default for $PXx<Output<MODE>> {}

            impl<MODE> InputPin for $PXx<Output<MODE>> {
                type Error = ();

                fn is_high(&self) -> Result<bool, Self::Error> {
                    let is_high = !self.is_low()?;
                    Ok(is_high)
                }

                fn is_low(&self) -> Result<bool, Self::Error> {
                    // NOTE(unsafe) atomic read with no side effects
                    let is_low = unsafe { (*pac::$GPIOX::ptr()).idr.read().bits() & (1 << self.i) == 0 };
                    Ok(is_low)
                }
            }

            impl<MODE> InputPin for $PXx<Input<MODE>> {
                type Error = ();

                fn is_high(&self) -> Result<bool, Self::Error> {
                    let is_high = !self.is_low()?;
                    Ok(is_high)
                }

                fn is_low(&self) -> Result<bool, Self::Error> {
                    // NOTE(unsafe) atomic read with no side effects
                    let is_low = unsafe { (*pac::$GPIOX::ptr()).idr.read().bits() & (1 << self.i) == 0 };
                    Ok(is_low)
                }
            }

            $(
                /// Pin
                pub struct $PXi<MODE> {
                    _mode: PhantomData<MODE>,
                }

                impl<MODE> $PXi<MODE> {
                    /// The port this pin is part of.
                    pub const PORT: Port = Port::$PXx;

                    /// The pin's number inside its port.
                    pub const PIN_NUMBER: u8 = $i;

                    /// Returns the port this pin is part of.
                    pub fn port(&self) -> Port {
                        Port::$PXx
                    }

                    /// Returns this pin's number inside its port.
                    pub fn pin_number(&self) -> u8 {
                        $i
                    }
                }

                impl<MODE> $PXi<MODE> {
                    /// Configures the pin to operate as a floating input pin
                    pub fn into_floating_input(
                        self,
                    ) -> $PXi<Input<Floating>> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the pin to operate as a pulled down input pin
                    pub fn into_pull_down_input(
                        self,
                        ) -> $PXi<Input<PullDown>> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b10 << offset))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the pin to operate as a pulled up input pin
                    pub fn into_pull_up_input(
                        self,
                    ) -> $PXi<Input<PullUp>> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b01 << offset))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the pin to operate as an analog pin
                    pub fn into_analog(
                        self,
                    ) -> $PXi<Analog> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b11 << offset))
                            });
                        }
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the pin to operate as an open drain output pin
                    /// Output is initialized into High-Z output (set high)
                    pub fn into_open_drain_output(
                        self,
                    ) -> $PXi<Output<OpenDrain>> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                            // set output HIGH for high-Z default
                            let _ = &(*pac::$GPIOX::ptr()).bsrr.write(|w| {
                                w.bits(1 << $i)});
                            let _ = &(*pac::$GPIOX::ptr()).otyper.modify(|r, w| {
                                w.bits(r.bits() | (0b1 << $i))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b01 << offset))
                            });
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the pin to operate as an push pull output pin
                    pub fn into_push_pull_output(
                        self,
                    ) -> $PXi<Output<PushPull>> {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).pupdr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).otyper.modify(|r, w| {
                                w.bits(r.bits() & !(0b1 << $i))
                            });
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b01 << offset))
                            });
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Set pin speed
                    pub fn set_speed(self, speed: Speed) -> Self {
                        let offset = 2 * $i;
                        unsafe {
                            let _ = &(*pac::$GPIOX::ptr()).ospeedr.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | ((speed as u32) << offset))
                            });
                        };
                        self
                    }

                    /// Configure the pin into an alternate function mode.
                    pub fn into_alt_function<T: AltMode>(self) -> $PXi<T> {
                        let mode = T::mode();
                        let offset = 2 * $i;
                        let offset2 = 4 * $i;
                        unsafe {
                            if offset2 < 32 {
                                let _ = &(*pac::$GPIOX::ptr()).afrl.modify(|r, w| {
                                    w.bits((r.bits() & !(0b1111 << offset2)) | (mode << offset2))
                                });
                            } else {
                                let offset2 = offset2 - 32;
                                let _ = &(*pac::$GPIOX::ptr()).afrh.modify(|r, w| {
                                    w.bits((r.bits() & !(0b1111 << offset2)) | (mode << offset2))
                                });
                            }
                            let _ = &(*pac::$GPIOX::ptr()).moder.modify(|r, w| {
                                w.bits((r.bits() & !(0b11 << offset)) | (0b10 << offset))
                            });
                        }
                        $PXi {
                            _mode: PhantomData
                        }
                    }
                }

                impl<MODE> $PXi<Output<MODE>> {
                    /// Erases the pin number from the type
                    ///
                    /// This is useful when you want to collect the pins into an array where you
                    /// need all the elements to have the same type
                    pub fn downgrade(self) -> $PXx<Output<MODE>> {
                        $PXx {
                            i: $i,
                            _mode: self._mode,
                        }
                    }
                }

                impl<MODE> OutputPin for $PXi<Output<MODE>> {
                    type Error = ();

                    fn set_high(&mut self) -> Result<(), Self::Error> {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe { (*pac::$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << $i)) };
                        Ok(())
                    }

                    fn set_low(&mut self) -> Result<(), Self::Error> {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe { (*pac::$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << ($i + 16))) };
                        Ok(())
                    }
                }

                impl<MODE> StatefulOutputPin for $PXi<Output<MODE>> {

                    fn is_set_high(&self) -> Result<bool, Self::Error> {
                        let is_set_high = !self.is_set_low()?;
                        Ok(is_set_high)
                    }

                    fn is_set_low(&self) -> Result<bool, Self::Error> {
                        // NOTE(unsafe) atomic read with no side effects
                        let is_set_low = unsafe { (*pac::$GPIOX::ptr()).odr.read().bits() & (1 << $i) == 0 };
                        Ok(is_set_low)
                    }
                }

                impl<MODE> toggleable::Default for $PXi<Output<MODE>> {}

                impl<MODE> InputPin for $PXi<Output<MODE>> {
                    type Error = ();

                    fn is_high(&self) -> Result<bool, Self::Error> {
                        let is_high = !self.is_low()?;
                        Ok(is_high)
                    }

                    fn is_low(&self) -> Result<bool, Self::Error> {
                        // NOTE(unsafe) atomic read with no side effects
                        let is_low = unsafe { (*pac::$GPIOX::ptr()).idr.read().bits() & (1 << $i) == 0 };
                        Ok(is_low)
                    }
                }

                impl<MODE> $PXi<Input<MODE>> {
                    /// Erases the pin number from the type
                    ///
                    /// This is useful when you want to collect the pins into an array where you
                    /// need all the elements to have the same type
                    pub fn downgrade(self) -> $PXx<Input<MODE>> {
                        $PXx {
                            i: $i,
                            _mode: self._mode,
                        }
                    }
                }

                impl<MODE> InputPin for $PXi<Input<MODE>> {
                    type Error = ();

                    fn is_high(&self) -> Result<bool, Self::Error> {
                        let is_high = !self.is_low()?;
                        Ok(is_high)
                    }

                    fn is_low(&self) -> Result<bool, Self::Error> {
                        // NOTE(unsafe) atomic read with no side effects
                        let is_low = unsafe { (*pac::$GPIOX::ptr()).idr.read().bits() & (1 << $i) == 0 };
                        Ok(is_low)
                    }
                }
            )+
        }
    }
}

gpio!(GPIOA, gpioa, iopaen, PA, [
    PA0: (pa0, 0, Input<Floating>),
    PA1: (pa1, 1, Input<Floating>),
    PA2: (pa2, 2, Input<Floating>),
    PA3: (pa3, 3, Input<Floating>),
    PA4: (pa4, 4, Input<Floating>),
    PA5: (pa5, 5, Input<Floating>),
    PA6: (pa6, 6, Input<Floating>),
    PA7: (pa7, 7, Input<Floating>),
    PA8: (pa8, 8, Input<Floating>),
    PA9: (pa9, 9, Input<Floating>),
    PA10: (pa10, 10, Input<Floating>),
    PA11: (pa11, 11, Input<Floating>),
    PA12: (pa12, 12, Input<Floating>),
    PA13: (pa13, 13, Input<Floating>),
    PA14: (pa14, 14, Input<Floating>),
    PA15: (pa15, 15, Input<Floating>),
]);

gpio!(GPIOB, gpiob, iopben, PB, [
    PB0: (pb0, 0, Input<Floating>),
    PB1: (pb1, 1, Input<Floating>),
    PB2: (pb2, 2, Input<Floating>),
    PB3: (pb3, 3, Input<Floating>),
    PB4: (pb4, 4, Input<Floating>),
    PB5: (pb5, 5, Input<Floating>),
    PB6: (pb6, 6, Input<Floating>),
    PB7: (pb7, 7, Input<Floating>),
    PB8: (pb8, 8, Input<Floating>),
    PB9: (pb9, 9, Input<Floating>),
    PB10: (pb10, 10, Input<Floating>),
    PB11: (pb11, 11, Input<Floating>),
    PB12: (pb12, 12, Input<Floating>),
    PB13: (pb13, 13, Input<Floating>),
    PB14: (pb14, 14, Input<Floating>),
    PB15: (pb15, 15, Input<Floating>),
]);

gpio!(GPIOC, gpioc, iopcen, PC, [
    PC0: (pc0, 0, Input<Floating>),
    PC1: (pc1, 1, Input<Floating>),
    PC2: (pc2, 2, Input<Floating>),
    PC3: (pc3, 3, Input<Floating>),
    PC4: (pc4, 4, Input<Floating>),
    PC5: (pc5, 5, Input<Floating>),
    PC6: (pc6, 6, Input<Floating>),
    PC7: (pc7, 7, Input<Floating>),
    PC8: (pc8, 8, Input<Floating>),
    PC9: (pc9, 9, Input<Floating>),
    PC10: (pc10, 10, Input<Floating>),
    PC11: (pc11, 11, Input<Floating>),
    PC12: (pc12, 12, Input<Floating>),
    PC13: (pc13, 13, Input<Floating>),
    PC14: (pc14, 14, Input<Floating>),
    PC15: (pc15, 15, Input<Floating>),
]);

gpio!(GPIOD, gpiod, iopden, PD, [
    PD0: (pd0, 0, Input<Floating>),
    PD1: (pd1, 1, Input<Floating>),
    PD2: (pd2, 2, Input<Floating>),
    PD3: (pd3, 3, Input<Floating>),
    PD4: (pd4, 4, Input<Floating>),
    PD5: (pd5, 5, Input<Floating>),
    PD6: (pd6, 6, Input<Floating>),
    PD7: (pd7, 7, Input<Floating>),
    PD8: (pd8, 8, Input<Floating>),
    PD9: (pd9, 9, Input<Floating>),
    PD10: (pd10, 10, Input<Floating>),
    PD11: (pd11, 11, Input<Floating>),
    PD12: (pd12, 12, Input<Floating>),
    PD13: (pd13, 13, Input<Floating>),
    PD14: (pd14, 14, Input<Floating>),
    PD15: (pd15, 15, Input<Floating>),
]);

gpio!(GPIOE, gpioe, iopeen, PE, [
    PE0: (pe0, 0, Input<Floating>),
    PE1: (pe1, 1, Input<Floating>),
    PE2: (pe2, 2, Input<Floating>),
    PE3: (pe3, 3, Input<Floating>),
    PE4: (pe4, 4, Input<Floating>),
    PE5: (pe5, 5, Input<Floating>),
    PE6: (pe6, 6, Input<Floating>),
    PE7: (pe7, 7, Input<Floating>),
    PE8: (pe8, 8, Input<Floating>),
    PE9: (pe9, 9, Input<Floating>),
    PE10: (pe10, 10, Input<Floating>),
    PE11: (pe11, 11, Input<Floating>),
    PE12: (pe12, 12, Input<Floating>),
    PE13: (pe13, 13, Input<Floating>),
    PE14: (pe14, 14, Input<Floating>),
    PE15: (pe15, 15, Input<Floating>),
]);

gpio!(GPIOH, gpioh, iophen, PH, [
    PH0: (ph0, 0, Input<Floating>),
    PH1: (ph1, 1, Input<Floating>),
    PH2: (ph2, 2, Input<Floating>),
    PH9: (ph9, 9, Input<Floating>),
    PH10: (ph10, 10, Input<Floating>),
]);
