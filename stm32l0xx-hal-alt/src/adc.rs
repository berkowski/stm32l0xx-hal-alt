//! ADC configuration and interface
//!
//! The ADC on the XRM is used for:
//! - Monitoring release feedback values
//! - Monitoring leak detection output values
//!
//! The Burnwire PWM signal timer is slaved to the ADC trigger timer in order to
//! accurately measure the average current delivered through the burnwire.

#[allow(unused_imports)]
use crate::{
    // dma::{
    //     adc::State,
    //     channel::{target, FromPeripheral, PeripheralChannel},
    // },
    gpio::{gpioa, gpiob, gpioc, Analog},
    pac::{
        self,
        adc::{cfgr1, cfgr2, smpr},
    },
    rcc::Rcc,
    Borrow,
};
use core::fmt::Formatter;

use embedded_hal::adc::Channel;
use embedded_hal_ext::{DisablePeripheral, EnablePeripheral};
use embedded_time::rate::Hertz;

use core::marker::PhantomData;

macro_rules! adc_pins {
    ($(($Gpio:ident, $Pin:ident, $Channel:expr),)+) => {

        mod pin_private {
            use super::*;
            pub trait Sealed {}

            $(
                impl Sealed for $Gpio::$Pin<Analog> {}
            )+
        }

        $(
            impl Channel<pac::ADC> for $Gpio::$Pin<Analog> {
                /// ADC channel number
                type ID = u8;

                #[inline(always)]
                fn channel() -> Self::ID {
                    $Channel
                }
            }

        )+
    };
}

adc_pins! {
(gpioa, PA0, 0),
(gpioa, PA1, 1),
(gpioa, PA2, 2),
(gpioa, PA3, 3),
(gpioa, PA4, 4),
(gpioa, PA5, 5),
(gpioa, PA6, 6),
(gpioa, PA7, 7),
(gpiob, PB0, 8),
(gpiob, PB1, 9),
(gpioc, PC0, 10),
(gpioc, PC1, 11),
(gpioc, PC2, 12),
(gpioc, PC3, 13),
(gpioc, PC4, 14),
(gpioc, PC5, 15),
}

/// Selected ADC input channels in ADC sequence
pub struct Channels<const N: usize>(u32);

impl<const N: usize> Channels<N> {
    /// Return true if channel index has been added.
    pub fn contains(&self, channel: u8) -> bool {
        (self.0 & (1 << channel)) != 0
    }
}

macro_rules! impl_channel {
    ($i:expr) => {
        impl Channels<{ $i }> {
            /// Add ADC channel to sequence
            pub fn and<T>(self, _pin: T) -> Channels<{ $i + 1 }>
            where
                T: Channel<pac::ADC, ID = u8>,
            {
                Channels::<{ $i + 1 }>(self.0 | 1u32 << T::channel())
            }
        }
    };
}

impl_channel! {0}
impl_channel! {1}
impl_channel! {2}
impl_channel! {3}
impl_channel! {4}
impl_channel! {5}
impl_channel! {6}
impl_channel! {7}
impl_channel! {8}
impl_channel! {9}
impl_channel! {10}
impl_channel! {11}
impl_channel! {12}
impl_channel! {13}
impl_channel! {14}
impl_channel! {15}
impl_channel! {16}
impl_channel! {17}
impl_channel! {18}

impl<T> From<T> for Channels<1>
where
    T: Channel<pac::ADC, ID = u8>,
{
    fn from(_: T) -> Self {
        Self(1u32 << T::channel())
    }
}

impl<const N: usize> From<Channels<N>> for u32 {
    fn from(ch: Channels<N>) -> u32 {
        ch.0
    }
}

// impl<const N: usize> Into<u32> for Channels<N> {
//     fn into(self) -> u32 {
//         self.0
//     }
// }

impl<const N: usize> AsRef<u32> for Channels<N> {
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

/// ADC Oversampling configuration
pub enum Oversample {
    /// Oversampling disabled
    Disabled,
    /// Oversampling enabled
    Enabled {
        /// Oversampling ratio
        ratio: cfgr2::OVSR_A,
        /// Ovesampling shift
        shift: u8,
        /// Oversampling trigger
        trigger: cfgr2::TOVS_A,
    },
}

/// ADC external trigger configuration
pub enum ExternalTrigger {
    /// Disabled
    Disabled,
    /// Enabled
    Enabled {
        /// Trigger source
        trigger: cfgr1::EXTSEL_A,
        /// Trigger edge
        edge: cfgr1::EXTEN_A,
    },
}

/// Watchdog channel selection
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum WatchdogChannel {
    /// All active ADC channels participate in watchdog
    All,
    /// Single given channel index participates in watchdog
    Single(u8),
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Watchdog {
    /// Watchdog disabled
    Disabled,
    /// Watchdog Enabled
    Enabled {
        channel: WatchdogChannel,
        low: u16,
        high: u16,
    },
}

/// ADC Clock mode
pub type ClockMode = cfgr2::CKMODE_A;

/// ADC Sampling mode
pub type ContinuousMode = cfgr1::CONT_A;

/// ADC Sample time
pub type SampleTime = smpr::SMP_A;

/// ADC Alignment
pub type Alignment = cfgr1::ALIGN_A;

/// Auto-Off
pub type AutoOff = cfgr1::AUTOFF_A;

/// Sample resolution
pub type SampleResolution = cfgr1::RES_A;

/// Scan direction
pub type ScanDirection = cfgr1::SCANDIR_A;

/// ADC peripheral config
pub struct Config {
    /// ADC Sampling mode
    pub continuous: ContinuousMode,

    /// Sample time
    pub sample_time: SampleTime,

    /// Data Alignment
    pub alignment: Alignment,

    /// Auto-Off
    pub auto_off: AutoOff,

    /// Sample resolution
    pub resolution: SampleResolution,

    /// Scan direction
    pub scan_direction: ScanDirection,

    /// Oversampler configuration
    pub oversample: Oversample,

    /// External trigger configuration
    pub external_trigger: ExternalTrigger,

    /// ADC Watchdog configuration
    pub watchdog: Watchdog,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            continuous: cfgr1::CONT_A::Single,
            sample_time: smpr::SMP_A::Cycles15,
            alignment: cfgr1::ALIGN_A::Right,
            auto_off: cfgr1::AUTOFF_A::Disabled,
            resolution: cfgr1::RES_A::TwelveBit,
            scan_direction: cfgr1::SCANDIR_A::Upward,
            oversample: Oversample::Disabled,
            external_trigger: ExternalTrigger::Disabled,
            watchdog: Watchdog::Disabled,
        }
    }
}

// pub struct OversampleConfig {
//     pub ratio: cfgr2::OVSR_A,
//     pub triggering: cfgr2::TOVS_A,
//     pub enabled: cfgr2::OVSE_A,
// }

// impl Default for OversampleConfig {
//     fn default() -> Self {
//         Self {
//             ratio: cfgr2::OVSR_A::MUL2,
//             triggering: cfgr2::TOVS_A::TRIGGERALL,
//             enabled: cfgr2::OVSE_A::DISABLED,
//         }
//     }
// }

bitflags::bitflags! {
/// ADC interrupt events
pub struct Interrupt: u32 {
    /// End of calibration
    const END_OF_CALIBRATION = 1 << 11;
    /// Analog watchdog
    const ANALOG_WATCHDOG = 1 << 7;
    /// Overrun
    const OVERRUN = 1 << 4;
    /// End of sequence
    const END_OF_SEQUENCE = 1 << 3;
    /// End of conversion
    const END_OF_CONVERSION = 1 << 2;
    /// End of sampling
    const END_OF_SAMPLING = 1 << 1;
    /// ADC Ready
    const READY = 1;
}
}

/// ADC error kinds
#[derive(Debug)]
pub enum Error<const N: usize> {
    /// If we use the peripheral clock then we need to ensure the AHB and APB pre-scalars
    /// are bypassed (set to 1).  See section 13.3.5 in manual
    InvalidClockMode(Adc<Unconfigured, { N }>),

    /// If the provided watchdog channel is not in the set of channels used in ADC sampling
    InvalidWatchdogChannel(Adc<Unconfigured, { N }>),
}

/// ADC is configured and ready to start converting samples
pub struct Ready {}
/// ADC conversion started
pub struct Started {}
/// ADC is unconfigured
pub struct Unconfigured {}

pub trait AdcExt<const N: usize> {
    fn constrain(
        self,
        channels: Channels<N>,
        clock_mode: cfgr2::CKMODE_A,
        rcc: &mut Rcc,
    ) -> Result<Adc<Unconfigured, N>, Error<{ N }>>;
}

impl<const N: usize> AdcExt<N> for pac::ADC {
    fn constrain(
        self,
        channels: Channels<N>,
        clock_mode: cfgr2::CKMODE_A,
        rcc: &mut Rcc,
    ) -> Result<Adc<Unconfigured, N>, Error<{ N }>> {
        // Enable & reset the ADC peripheral
        rcc.rcc.apb2rstr.modify(|_, w| w.adcrst().reset());
        rcc.rcc.apb2rstr.modify(|_, w| w.adcrst().clear_bit());
        rcc.rcc.apb2enr.modify(|_, w| w.adcen().enabled());

        use pac::adc::cfgr2::CKMODE_A;

        // If we use the peripheral clock then we need to ensure the AHB and APB pre-scalars
        // are bypassed (set to 1).  See section 13.3.5 in manual
        if CKMODE_A::Pclk == clock_mode
            && (rcc.clocks.ahb_clk() != rcc.clocks.apb2_clk()
                || rcc.clocks.sys_clk() != rcc.clocks.ahb_clk())
        {
            return Err(Error::InvalidClockMode(Adc {
                adc: self,
                channels,
                state: PhantomData {},
            }));
        }

        // enable HSI16 clock if needed
        if CKMODE_A::Adclk == clock_mode {
            rcc.rcc.cr.modify(|_, w| w.hsi16on().enabled());
        }

        // Get ADC clock speed
        let adc_clock_freq = match clock_mode {
            CKMODE_A::Adclk => {
                if rcc.rcc.cr.read().hsi16diven().is_div4() {
                    Hertz(4_000_000u32)
                } else {
                    Hertz(16_000_000u32)
                }
            }
            CKMODE_A::Pclk => rcc.clocks.apb2_clk(),
            CKMODE_A::PclkDiv2 => rcc.clocks.apb2_clk() / 2,
            CKMODE_A::PclkDiv4 => rcc.clocks.apb2_clk() / 4,
        };

        // Enable low frequency mode if needed
        if adc_clock_freq < Hertz(3_500_000u32) {
            self.ccr.modify(|_, w| w.lfmen().enabled());
        }

        self.cfgr2.modify(|_, w| w.ckmode().variant(clock_mode));
        Ok(Adc {
            adc: self,
            channels,
            state: PhantomData {},
        })
    }
}

/// ADC interface
pub struct Adc<T, const N: usize> {
    /// ADC peripheral
    pub(crate) adc: pac::ADC,
    state: PhantomData<T>,
    channels: Channels<N>,
}

impl<const N: usize> core::fmt::Debug for Adc<Unconfigured, N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Adc")
            .field("state", &"Unconfigured")
            .field("channels", &N)
            .finish()
    }
}

impl<const N: usize> core::fmt::Debug for Adc<Ready, N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Adc")
            .field("state", &"Ready")
            .field("channels", &N)
            .finish()
    }
}

impl<const N: usize> core::fmt::Debug for Adc<Started, N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Adc")
            .field("state", &"Started")
            .field("channels", &N)
            .finish()
    }
}

impl<T, const N: usize> Adc<T, N> {
    /// Gain access to the underlying ADC peripherial
    ///
    /// As this HAL doesn't provide full high-level access ot all
    /// peripherals the user may need access to the ADC for their own
    /// configuration/setup needs.
    ///
    /// # Safety
    /// Modifying the ADC in this manner carries the risk of invalidating
    /// all other assumptions this HAL provides.  Users should be *extremely*
    /// cautious when manipulating the bare ADC registers.
    pub unsafe fn with_raw<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&pac::ADC) -> R,
    {
        f(&self.adc)
    }

    /// Set active interrupts
    pub fn enable_interrupts(&mut self, interrupts: Interrupt) {
        unsafe {
            self.adc
                .ier
                .modify(|r, w| w.bits(r.bits() | interrupts.bits()))
        };
    }

    pub fn disable_interrupts(&mut self, interrupts: Interrupt) {
        unsafe {
            self.adc
                .ier
                .modify(|r, w| w.bits(r.bits() & (!interrupts.bits())))
        };
    }

    /// Get active interrupts
    pub fn interrupts(&self) -> Interrupt {
        Interrupt::from_bits_truncate(self.adc.ier.read().bits())
    }

    /// Check for pending events
    ///
    /// Does not clear events, to do so use clear()
    pub fn pending(&self) -> Option<Interrupt> {
        let events = Interrupt::from_bits_truncate(self.adc.isr.read().bits());
        if events.is_empty() {
            None
        } else {
            Some(events)
        }
    }

    /// Clear pending events
    pub fn clear(&mut self, interrupts: Interrupt) {
        self.adc.isr.write(|w| unsafe { w.bits(interrupts.bits()) });
    }
}

impl<const N: usize> Adc<Unconfigured, N> {
    /// Configure the ADC peripheral
    pub fn configure(self, config: &Config) -> Result<Adc<Ready, N>, Error<{ N }>> {
        // Sample timing setup
        self.adc.smpr.write(|w| w.smp().variant(config.sample_time));

        let (oversampler_enabled, ratio, shift, trigger) = match config.oversample {
            Oversample::Disabled => (
                cfgr2::OVSE_A::Disabled,
                cfgr2::OVSR_A::Mul2,
                0,
                cfgr2::TOVS_A::TriggerAll,
            ),
            Oversample::Enabled {
                ratio,
                shift,
                trigger,
            } => (cfgr2::OVSE_A::Enabled, ratio, shift, trigger),
        };

        let (watchdog_enabled, watchdog_sel, watchdog_ch, watchdog_low, watchdog_high) =
            match config.watchdog {
                Watchdog::Disabled => (
                    cfgr1::AWDEN_A::Disabled,
                    cfgr1::AWDSGL_A::AllChannels,
                    0,
                    0,
                    0,
                ),
                Watchdog::Enabled { channel, low, high } => match channel {
                    WatchdogChannel::All => (
                        cfgr1::AWDEN_A::Enabled,
                        cfgr1::AWDSGL_A::AllChannels,
                        0,
                        low,
                        high,
                    ),
                    WatchdogChannel::Single(ch) => (
                        cfgr1::AWDEN_A::Enabled,
                        cfgr1::AWDSGL_A::SingleChannel,
                        ch,
                        low,
                        high,
                    ),
                },
            };

        if watchdog_sel == cfgr1::AWDSGL_A::SingleChannel && !self.channels.contains(watchdog_ch) {
            return Err(Error::InvalidWatchdogChannel(self));
        }

        // Set up CFGR1
        #[rustfmt::skip]
        self.adc.cfgr1.modify(|_, w| {
            let (source, trigger) = match config.external_trigger {
                ExternalTrigger::Disabled => (cfgr1::EXTEN_A::Disabled, cfgr1::EXTSEL_A::Tim6Trgo),
                ExternalTrigger::Enabled { trigger, edge } => (edge, trigger),
            };

	    // unsafe needed for awdch value
	    unsafe{
            w.autoff().variant(config.auto_off)
                .awdch().bits(watchdog_ch)
                .awdsgl().variant(watchdog_sel)
                .awden().variant(watchdog_enabled)
                .align().variant(config.alignment)
                .scandir().variant(config.scan_direction)
                .res().variant(config.resolution)
                .cont().variant(config.continuous)
                .extsel().variant(trigger)
                .exten().variant(source)
	    }
        });

        let wd_shift = if oversampler_enabled == cfgr2::OVSE_A::Enabled {
            shift
        } else {
            0
        };

        self.adc.tr.write(|w| unsafe {
            w.lt()
                .bits((watchdog_low & 0x0FFF) >> wd_shift)
                .ht()
                .bits((watchdog_high & 0x0FFF) >> wd_shift)
        });

        // Set up CFGR2
        #[rustfmt::skip]
        self.adc.cfgr2.modify(|_, w| {
            // need unsafe block to use ovss().bits()
            unsafe { w.ovss().bits(shift)}
                    .ovse().variant(oversampler_enabled)
                    .ovsr().variant(ratio)
                    .tovs().variant(trigger)
        });

        // Now set the desired analog channels.
        unsafe {
            self.adc.chselr.write(|w| w.bits(*self.channels.as_ref()));
        }

        // ADC calibration procedure specified in 13.3.3 of manual.  Only needs to be done
        // after reset (disabling peripheral doesn't reset calibration)
        self.adc.cr.modify(|_, w| w.adcal().set_bit());

        // Wait for end of calibration flag to be set, then clear it
        while self.adc.isr.read().eocal().bit_is_clear() {}
        self.adc.isr.modify(|_, w| w.eocal().set_bit());

        // Configure interrupts
        //
        // End of Sequence (EOS) interrupts are used to switch the analog mux to the
        // next release channel
        // adc.ier.modify(|_, w| w.eosie().enabled());

        Ok(Adc {
            adc: self.adc,
            channels: self.channels,
            state: PhantomData {},
        })
    }
}

impl<const N: usize> Adc<Started, N> {
    /// Stop ADC conversion
    ///
    /// Stops any pending conversion and resets ADC and dma transfer channel.
    pub fn stop(self) -> Adc<Ready, N> {
        // Stop pending ADC conversion
        if self.adc.cr.read().adstart().bit_is_set() {
            self.adc.cr.modify(|_, w| w.adstp().stop_conversion());
            while self.adc.cr.read().adstart().bit_is_set() {}
        }

        Adc {
            adc: self.adc,
            state: PhantomData {},
            channels: self.channels,
        }
        // Reset DMA
        // Disable DMA channel
        // let ch = self.ch.take().unwrap().disable(handle);

        // Re-enable DMA channel
        // self.ch.replace(ch.enable());
    }
}

impl<const N: usize> Adc<Ready, N> {
    /// Start ADC conversion
    ///
    /// Stops any pending conversion and resets ADC and dma transfer channel.
    pub fn start(mut self) -> Adc<Started, N> {
        self.enable().ok();

        self.adc.cr.modify(|_, w| w.adstart().start_conversion());
        while !self.adc.cr.read().adstart().bit_is_set() {}

        Adc {
            adc: self.adc,
            channels: self.channels,
            state: PhantomData {},
        }
    }

    pub fn reconfigure(self, config: &Config) -> Result<Adc<Ready, N>, Error<{ N }>> {
        let adc: Adc<Unconfigured, N> = Adc {
            adc: self.adc,
            state: PhantomData {},
            channels: self.channels,
        };
        adc.configure(config)
    }

    // pub fn into_dma<Ch, const N: usize>(
    //     mut self,
    //     mut channel: Ch,
    //     buffer: Pin<&'static [u16; N]>,
    //     dma: &mut crate::dma::Dma1,
    // ) -> crate::dma::adc::Adc<Ch, &'static mut [u16; N], Stopped>
    // where
    //     Ch: PeripheralChannel<Target = target::ADC, Direction = FromPeripheral, WordSize = BITS16>,
    // {
    //     self.disable();
    //     channel.disable().ok();
    //     channel.set_memory_address(buffer.deref().as_ptr());

    //     // Enable DMA
    //     self.adc.cfgr1.modify(|_, w| w.dmaen().enabled());

    //     crate::dma::adc::Adc {
    //         adc: self.adc,
    //         state: State::Ready { channel, buffer },
    //     }
    // }
}

impl<T, const N: usize> Borrow<pac::ADC> for Adc<T, N> {
    fn borrow(&self) -> &pac::ADC {
        &self.adc
    }
}

impl<const N: usize> EnablePeripheral for Adc<Ready, N> {
    type Error = core::convert::Infallible;

    #[inline(always)]
    fn enable(&mut self) -> Result<(), Self::Error> {
        if !self.is_enabled().unwrap_or(false) {
            self.adc.isr.write(|w| w.adrdy().clear());
            self.adc.cr.modify(|_, w| w.aden().enabled());
            // Enable the ADC.  Do NOT need to wait for the ADRDY flag to if we're using
            // Auto-Off mode.
            if !self.adc.cfgr1.read().autoff().is_enabled() {
                while self.adc.isr.read().adrdy().is_not_ready() {}
            }
        }
        Ok(())
    }

    #[inline(always)]
    fn is_enabled(&self) -> Result<bool, Self::Error> {
        Ok(self.adc.cr.read().aden().is_enabled())
    }
}

impl<const N: usize> DisablePeripheral for Adc<Ready, N> {
    #[inline(always)]
    fn disable(&mut self) -> Result<(), Self::Error> {
        self.adc.cr.modify(|_, w| w.addis().disable());
        Ok(())
    }
}
