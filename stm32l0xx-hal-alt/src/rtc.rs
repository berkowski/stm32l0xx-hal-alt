//! Real Time Clock peripheral
//!

use crate::{
    pac,
    pwr::Pwr,
    rcc::{Lse, Lsi, Rcc, Ready},
};
use embedded_time::rate::Hertz;

use core::convert::TryFrom;

/// RTC Alarm
pub enum Alarm {
    /// RTC Alarm A
    A,
    /// RTC Alarm B
    B,
}

bitflags::bitflags! {
/// RTC Alarm match mask
pub struct AlarmMask: u8 {
    /// All subfields participate in alarm
    const IGNORE_NONE = 0x00;
    /// mask seconds
    const IGNORE_SECOND = 0x01;
    /// mask minutes
    const IGNORE_MINUTE = 0x02;
    /// mask hours
    const IGNORE_HOUR =  0x04;
    /// mask date
    const IGNORE_DATE = 0x08;
}
}

bitflags::bitflags! {
/// RTC Interrupt events
pub struct Event: u32 {
    /// Tamper 3 detection interrupt
    const TAMPER_3 = 1 << 15;
    /// Tamper 2 detection interrupt
    const TAMPER_2 = 1 << 14;
    /// Tamper 1 detection interrupt
    const TAMPER_1 = 1 << 13;
    /// Timestamp event occoured while a timestamp event was pending
    const TIMESTAMP_OVERFLOW = 1 << 12;
    /// Timestamp event
    const TIMESTAMP = 1 << 11;
    /// Wakeup timer
    const WAKEUP = 1 << 10;
    /// Alaram B
    const ALARM_B = 1 << 9;
    /// Alarm A
    const ALARM_A = 1 << 8;
    /// Register Sync
    const REGISTER_SYNC = 1 << 5;
}
}
/// RTC Clock Source
pub enum ClockSource<'a> {
    /// Low-speed External oscillator with drive level
    LSE(&'a Lse<Ready>),
    /// Low-speed Internal oscillator
    LSI(&'a Lsi<Ready>, LSIFreq),
}

pub enum LSIFreq {
    Default,
    Other(Hertz<u32>),
}

impl Default for LSIFreq {
    fn default() -> Self {
        Self::Default
    }
}

fn find_rtc_prescale_values(lsi_freq: &Hertz<u32>) -> (u16, u8) {
    let lsi_freq = lsi_freq.0 as u32;
    let mut best_pre_s = 0u16;
    let mut best_pre_a = 0u8;
    let mut best_freq_dev = u32::MAX;

    for pre_a in 100..=127u8 {
        for pre_s in 250..=450u16 {
            let this_freq = (pre_s as u32 + 1) * (pre_a as u32 + 1);
            let freq_dev = match this_freq.cmp(&lsi_freq) {
                core::cmp::Ordering::Greater => this_freq - lsi_freq,
                _ => lsi_freq - this_freq,
            };

            if freq_dev < best_freq_dev {
                best_pre_a = pre_a;
                best_pre_s = pre_s;
                best_freq_dev = freq_dev;
            }
        }
    }
    (best_pre_s, best_pre_a)
}

/// Real Time Clock peripheral
pub struct Rtc {
    rtc: pac::RTC,
}

impl Rtc {
    /// Return the RTC peripheral
    pub fn free(self) -> pac::RTC {
        self.rtc
    }

    /// Constrain the RTC peripherial to use the desired clock source.
    ///
    /// NOTE: Unlike many other peripherals, this method does NOT reset the
    /// RTC peripheral.  By not reseting the peripheral we can check to see
    /// if the time has already been set.  This may be useful if recovering
    /// from a brown-out reset where the microcontroller has reset but the
    /// RTC register values have been retained.
    pub fn constrain(src: ClockSource<'_>, rtc: pac::RTC, rcc: &mut Rcc, pwr: &mut Pwr) -> Self {
        // TODO: Figure out what we want to do here.
        // Calling reset requires the LSE to re-ready, but we already asume LSE is ready
        // givine the provided clock source enum
        //  unsafe {
        //     rcc.with_raw(|handle| {
        //         handle.csr.modify(|_, w| w.rtcrst().set_bit());
        //         handle.csr.modify(|_, w| w.rtcrst().clear_bit());
        //     })
        // };
        // Disable backup protection.  Needed to enable LSE and work with RTC registers in general
        pwr.disable_backup_protection();

        let (prediv_s, prediv_a) = match src {
            ClockSource::LSE(_) => {
                rcc.rcc
                    .csr
                    .modify(|_, w| w.rtcsel().lse().rtcen().enabled());
                (255, 127)
            }
            ClockSource::LSI(_, freq) => {
                rcc.rcc
                    .csr
                    .modify(|_, w| w.rtcsel().lsi().rtcen().enabled());
                match freq {
                    LSIFreq::Default => (288, 127),
                    LSIFreq::Other(lsi_freq) => find_rtc_prescale_values(&lsi_freq),
                }
            }
        };
        defmt::debug!(
            "using rtc prediv_s: {=u16}, prediv_a: {=u8}",
            prediv_s,
            prediv_a
        );

        // Unlock RTC registers (section 22.4.7 of manual)
        // This is safe, this bit sequence is defined by the manual.
        #[allow(unsafe_code)]
        unsafe {
            rtc.wpr.write(|w| w.bits(0xCA));
            rtc.wpr.write(|w| w.bits(0x53));
        }

        // Enter INIT mode
        rtc.isr.modify(|_, w| w.init().set_bit());
        while rtc.isr.read().initf().bit_is_clear() {}

        // Set the prescaler values to create a 1Hz calender update
        // This is safe, the prescaler values are defined in section 22.4 of the manual
        #[allow(unsafe_code)]
        unsafe {
            rtc.prer
                .modify(|_, w| w.prediv_a().bits(prediv_a).prediv_s().bits(prediv_s));
        }

        // Set twenty-four hour format.
        rtc.cr.modify(|_, w| w.fmt().twenty_four_hour());

        // Exit init mode
        rtc.isr.modify(|_, w| w.init().clear_bit());

        // Lock RTC registers (section 22.4.7 of manual)
        // This is safe, this bit sequence is defined by the manual.
        #[allow(unsafe_code)]
        unsafe {
            rtc.wpr.write(|w| w.bits(0xFF));
        }

        Rtc { rtc }
    }

    pub fn datetime_from_registers(
        tr: &pac::rtc::tr::R,
        dr: &pac::rtc::dr::R,
    ) -> Result<time::PrimitiveDateTime, time::error::ComponentRange> {
        let month = time::Month::try_from(10 * dr.mt().bit() as u8 + dr.mu().bits())?;

        time::Date::from_calendar_date(
            2000i32 + 10i32 * dr.yt().bits() as i32 + dr.yu().bits() as i32,
            month,
            10 * dr.dt().bits() + dr.du().bits(),
        )
        .and_then(|date| {
            date.with_hms(
                10 * tr.ht().bits() + tr.hu().bits(),
                10 * tr.mnt().bits() + tr.mnu().bits(),
                10 * tr.st().bits() + tr.su().bits(),
            )
        })
    }

    /// Get the current RTC time
    ///
    /// ## Errors
    /// Returns an error if the RTC time as not been set
    pub fn get_time(&self) -> Option<time::PrimitiveDateTime> {
        if !self.is_clock_set() {
            return None;
        }

        let sr = self.rtc.isr.read();
        // INITS flag 0 means RTC calendar has not yet been initialized
        if sr.inits().bit_is_clear() {
            return None;
        }

        let tr = self.rtc.tr.read();
        let dr = self.rtc.dr.read();

        Self::datetime_from_registers(&tr, &dr).ok()
    }

    pub fn set_source_frequency(&mut self, freq: &Hertz<u32>) {
        let (prediv_s, prediv_a) = find_rtc_prescale_values(freq);
        self.with_unlocked(|self_| {
            self_.rtc.isr.modify(|_, w| w.init().set_bit());
            while self_.rtc.isr.read().initf().bit_is_clear() {}
            // Set the prescaler values to create a 1Hz calender update based on the source
            // clock frequency provided
            #[allow(unsafe_code)]
            unsafe {
                self_
                    .rtc
                    .prer
                    .modify(|_, w| w.prediv_a().bits(prediv_a).prediv_s().bits(prediv_s));
            };

            // exit init mode
            self_.rtc.isr.modify(|_, w| w.init().clear_bit());
            while self_.rtc.isr.read().rsf().bit_is_clear() {}
        });
        defmt::info!(
            "rtc prescale values for {}Hz: s:{} a:{}",
            freq.0,
            prediv_s,
            prediv_a
        );
    }

    /// Set the RTC time
    pub fn set_time(&mut self, date_time: &time::PrimitiveDateTime) {
        let (year, month, day) = date_time.date().to_calendar_date();
        let (hour, minute, second) = date_time.time().as_hms();
        let year = (year % 100) as u8;
        let month = month as u8;

        // Unlock and wait for RTC to enter initialization mode
        self.with_unlocked(|self_| {
            self_.rtc.isr.modify(|_, w| w.init().set_bit());
            while self_.rtc.isr.read().initf().bit_is_clear() {}

            // Load time and date registers
            #[rustfmt::skip]
            self_.rtc.tr.write(|w| {
                w.ht().bits(hour / 10)
                    .hu().bits(hour % 10)
                    .mnt().bits(minute / 10)
                    .mnu().bits(minute % 10)
                    .st().bits(second / 10)
                    .su().bits(second % 10)
            });

            #[rustfmt::skip]
            self_.rtc.dr.write(|w| {
                w.yt().bits(year / 10)
                    .yu().bits(year % 10)
                    .mt().bit(month > 10)
                    .mu().bits(month % 10)
                    .dt().bits(day / 10)
                    .du().bits(day % 10)
            });

            // Exit initialization mode and wait for shadow registers
            self_.rtc.isr.modify(|_, w| w.init().clear_bit());
            while self_.rtc.isr.read().rsf().bit_is_clear() {}
        });
    }

    fn with_unlocked<F>(&mut self, f: F)
    where
        F: FnOnce(&mut Self),
    {
        // Unlock RTC registers (section 22.4.7 of manual)
        // This is safe, this bit sequence is defined by the manual.
        #[allow(unsafe_code)]
        unsafe {
            self.rtc.wpr.write(|w| w.bits(0xCA));
            self.rtc.wpr.write(|w| w.bits(0x53));
        }

        f(self);

        // lock RTC registers - this byte sequence is used in Appendix A examples of the manual
        // This is safe, this bit sequence is defined by the manual.
        #[allow(unsafe_code)]
        unsafe {
            self.rtc.wpr.write(|w| w.bits(0xFF));
            // self.rtc.wpr.write(|w| w.bits(0x64));
        }
    }

    /// Check if the real-time clock has be initialized with a valid time
    #[inline(always)]
    pub fn is_clock_set(&self) -> bool {
        self.rtc.isr.read().inits().bit_is_set()
    }

    /// Set and listen for interrupts generated by an RTC alarm
    ///
    /// The mask bits determine when an the provided date_time is considered a "match".
    /// For example, to trigger an alarm on "Every Minute" you'd use:
    /// `AlarmMask::IGNORE_HOUR | AlarmMask::IGNORE_DATE` and a date_time with second = 0
    pub fn listen(&mut self, alarm: Alarm, date_time: &time::PrimitiveDateTime, mask: AlarmMask) {
        let day = date_time.day();
        let hour = date_time.hour();
        let minute = date_time.minute();
        let second = date_time.second();

        self.with_unlocked(|self_| match alarm {
            Alarm::A => {
                self_.rtc.cr.modify(|_, w| w.alrae().clear_bit());
                self_.rtc.isr.modify(|_, w| w.alraf().clear_bit());
                while self_.rtc.isr.read().alrawf().bit_is_clear() {}

                #[rustfmt::skip]
                self_.rtc.alrmar().write(|w| {
                    w.su().bits(second % 10)
                        .st().bits(second / 10)
                        .msk1().bit(mask.contains(AlarmMask::IGNORE_SECOND))
                        .mnu().bits(minute % 10)
                        .mnt().bits(minute / 10)
                        .msk2().bit(mask.contains(AlarmMask::IGNORE_MINUTE))
                        .hu().bits(hour % 10)
                        .ht().bits(hour / 10)
                        .msk3().bit(mask.contains(AlarmMask::IGNORE_HOUR))
                        .du().bits(day % 10)
                        .dt().bits(day / 10)
                        .msk4().bit(mask.contains(AlarmMask::IGNORE_DATE))
                });

                self_
                    .rtc
                    .cr
                    .modify(|_, w| w.alraie().enabled().alrae().enabled());
            }
            Alarm::B => {
                self_.rtc.cr.modify(|_, w| w.alrbe().clear_bit());
                self_.rtc.isr.modify(|_, w| w.alrbf().clear_bit());
                while self_.rtc.isr.read().alrbwf().bit_is_clear() {}
                #[rustfmt::skip]
                self_.rtc.alrmbr().write(|w| {
                    w.su().bits(second % 10)
                        .st().bits(second / 10)
                        .msk1().bit(mask.contains(AlarmMask::IGNORE_SECOND))
                        .mnu().bits(minute % 10)
                        .mnt().bits(minute / 10)
                        .msk2().bit(mask.contains(AlarmMask::IGNORE_MINUTE))
                        .hu().bits(hour % 10)
                        .ht().bits(hour / 10)
                        .msk3().bit(mask.contains(AlarmMask::IGNORE_HOUR))
                        .du().bits(day % 10)
                        .dt().bits(day / 10)
                        .msk4().bit(mask.contains(AlarmMask::IGNORE_DATE))
                });

                self_
                    .rtc
                    .cr
                    .modify(|_, w| w.alrbie().enabled().alrbe().enabled());
            }
        });
    }

    /// Disable alarm.
    pub fn unlisten(&mut self, alarm: Alarm) {
        self.with_unlocked(|self_| match alarm {
            Alarm::A => self_
                .rtc
                .cr
                .modify(|_, w| w.alraie().disabled().alrae().disabled()),
            Alarm::B => self_
                .rtc
                .cr
                .modify(|_, w| w.alrbie().disabled().alrbe().disabled()),
        })
    }

    /// Check for pending events
    ///
    /// Does not clear events, to do so use clear()
    pub fn pending(&self) -> Option<Event> {
        let events = Event::from_bits_truncate(self.rtc.isr.read().bits());
        if events.is_empty() {
            None
        } else {
            Some(events)
        }
    }

    /// Clear pending events
    pub fn clear(&mut self, event: Event) {
        // Safe, only valid Write-0-clear bits defined for Event bitflag
        self.rtc
            .isr
            .modify(|r, w| unsafe { w.bits(r.bits() & !(event.bits())) });
    }
}
