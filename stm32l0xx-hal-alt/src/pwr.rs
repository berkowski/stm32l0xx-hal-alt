//! Power control

use crate::{pac::PWR, rcc::Rcc};

/// Power controll peripheral
pub struct Pwr {
    pwr: PWR,
}

/// Voltage scaling range selection
#[repr(u8)]
pub enum VoltageScalingRange {
    /// 1.8V (range 1)
    Range1 = 0b01,
    /// 1.5V (range 2)
    Range2 = 0b10,
    /// 1.2V (range 3)
    Range3 = 0b11,
}

/// Invalid voltage scaling range
pub struct InvalidVoltageScalingRange {}

impl TryFrom<u8> for VoltageScalingRange {
    type Error = InvalidVoltageScalingRange;
    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            0b01 => Ok(Self::Range1),
            0b10 => Ok(Self::Range2),
            0b11 => Ok(Self::Range3),
            _ => Err(InvalidVoltageScalingRange {}),
        }
    }
}

impl Into<u8> for VoltageScalingRange {
    fn into(self) -> u8 {
        self as u8
    }
}

impl Pwr {
    pub fn constrain(pwr: PWR, rcc: &mut Rcc) -> Self {
        unsafe {
            rcc.with_raw(|handle| {
                handle.apb1rstr.modify(|_, w| w.pwrrst().set_bit());
                handle.apb1rstr.modify(|_, w| w.pwrrst().clear_bit());
                handle.apb1enr.modify(|_, w| w.pwren().set_bit());
            })
        }

        Self { pwr }
    }

    /// Gain access to the underlying PWR peripherial
    ///
    /// As this HAL doesn't provide full high-level access ot all
    /// peripherals the user may need access to the RCC for their own
    /// configuration/setup needs.
    ///
    /// # Safety
    /// Modifying the PWR in this manor carries the risk of invalidating
    /// all other assumptions this HAL provides.  Users should be *extremely*
    /// cautious when manipulating the bare RCC registers.
    pub unsafe fn with_raw<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&PWR) -> R,
    {
        f(&self.pwr)
    }

    /// Disable backup write protection
    ///
    /// In reset state, the RTC, RTC backup registers and RCC CSR register are protected against
    /// parasitic write access. This protection must be disabled to enable write access to these registers.
    pub fn disable_backup_protection(&mut self) {
        self.pwr.cr.modify(|_, w| w.dbp().set_bit());
        while self.pwr.cr.read().dbp().bit_is_clear() {}
    }

    /// Get the current Voltage Scaling Range
    pub fn voltage_range(&self) -> Result<VoltageScalingRange, InvalidVoltageScalingRange> {
        VoltageScalingRange::try_from(self.pwr.cr.read().vos().bits())
    }

    /// Set the voltage scaling range
    ///
    /// The dynamic voltage scaling is a power management technique which consists in
    /// increasing or decreasing the voltage used for the digital peripherals (VCORE), according to
    /// the circumstances.
    ///
    /// Dynamic voltage scaling to increase VCORE is known as overvolting. It allows improving the
    /// device performance. Refer to Figure 11 for a description of the device operating conditions
    /// versus CPU performance and to the datasheet electrical characteristics for ADC clock
    /// frequency versus dynamic range.
    ///
    /// Dynamic voltage scaling to decrease VCORE is known as undervolting. It is performed to
    /// save power, particularly in laptops and other mobile devices where the energy comes from a
    /// battery and is thus limited.
    ///
    /// # Warning
    /// This method blocks on the PWR_CSR::VOSF bit.
    ///
    /// # Range 1
    /// Range 1 is the “high performance” range.
    ///
    /// The voltage regulator outputs a 1.8 V voltage (typical) as long as the VDD input voltage is
    /// above 1.71 V. Flash program and erase operations can be performed in this range.
    /// When VDD is below 2.0 V, the CPU frequency changes from initial to final state must respect
    /// the following conditions:
    /// - fCPUfinal < 4xfCPUinitial.
    /// - In addition, a 5 μs delay must be respected between two changes. For example to
    ///   switch from 4.2 to 32 MHz, switch from 4.2 to 16 MHz, wait for 5 μs, then switch from
    ///   16 to 32 MHz.
    ///
    /// # Range 2 and 3
    /// The regulator can also be programmed to output a regulated 1.5 V (typical, range 2) or a
    /// 1.2 V (typical, range 3) without any limitations on VDD (1.65 to 3.6 V).
    ///
    /// - At 1.5 V, the Flash memory is still functional but with medium read access time. This is
    ///   the “medium performance” range. Program and erase operations on the Flash memory
    ///   are still possible.
    /// - At 1.2 V, the Flash memory is still functional but with slow read access time. This is the
    ///   “low performance” range. Program and erase operations on the Flash memory are not
    ///   possible under these conditions.
    pub fn set_voltage_scaling_range(&mut self, range: VoltageScalingRange) {
        while self.pwr.csr.read().vosf().bit_is_set() {}
        self.pwr
            .cr
            .modify(|_, w| unsafe { w.vos().bits(range.into()) });
        while self.pwr.csr.read().vosf().bit_is_set() {}
    }
}
