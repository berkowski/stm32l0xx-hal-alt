//! EXTI interrupt and event lines
//!
//! Modeled from stm32l0xx-hal crate: <https://github.com/stm32-rs/stm32l0xx-hal/blob/master/src/gpio.rs>

use crate::{pac::EXTI, rcc::Rcc};
use core::marker::PhantomData;

/// Enabled mode (type state)
pub struct Enabled<MODE> {
    _mode: PhantomData<MODE>,
}

/// Disabled mode (type state)
pub struct Disabled;

/// Rising Edge Trigger typestate
pub struct Rising;

/// Falling Edge Trigger type state
pub struct Falling;

/// Both Rising and Falling edge trigger typestate
pub struct Both;

macro_rules! exti {
    ($($PXi:ident: ($pxi:ident, $i:expr, $MODE:ty),)+) => {
            /// External interrupt lines
            pub struct Exti {
                $(
                    /// external interrupt
                    pub $pxi: $PXi<$MODE>,
                )+
            }

            impl Exti {
                /// Create the external interrupt line interface
                pub fn constrain(_: EXTI, _: &mut Rcc) -> Self {
                    Self {
                        $(
                            $pxi: $PXi {
                                _mode: PhantomData,
                            },
                        )+
                    }
                }
            }

            $(
                /// External Interrupt
                pub struct $PXi<MODE> {
                    _mode: PhantomData<MODE>,
                }

                impl<MODE> $PXi<MODE> {
                    /// The interrupt's line number
                    pub const LINE_NUMBER: u8 = $i;

                    /// Returns this interrupt's line number
                    pub fn line_number(&self) -> u8 {
                        Self::LINE_NUMBER
                    }
                }

                impl<MODE> $PXi<MODE> {
                    /// Disable the interrupt
                    pub fn into_disabled(
                        self,
                    ) -> $PXi<Disabled> {
                        unsafe {
                            let _ = &(*EXTI::ptr()).imr.modify(|r, w| w.bits(r.bits() & !(0b1 << $i)));
                            let _ = &(*EXTI::ptr()).rtsr.modify(|r, w| w.bits(r.bits() & !(0b1 << $i)));
                            let _ = &(*EXTI::ptr()).ftsr.modify(|r, w| w.bits(r.bits() & !(0b1 << $i)));
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the line to trigger interrupts on rising edges
                    pub fn into_rising_edge_triggered (
                        self,
                        ) -> $PXi<Enabled<Rising>> {
                        unsafe {
                            let _ = &(*EXTI::ptr()).imr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                            let _ = &(*EXTI::ptr()).rtsr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                            let _ = &(*EXTI::ptr()).ftsr.modify(|r, w| w.bits(r.bits() & !(0b1 << $i)));
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the line to trigger interrupts on falling edges
                    pub fn into_falling_edge_triggered (
                        self,
                        ) -> $PXi<Enabled<Falling>> {
                        unsafe {
                            let _ = &(*EXTI::ptr()).imr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                            let _ = &(*EXTI::ptr()).rtsr.modify(|r, w| w.bits(r.bits() & !(0b1 << $i)));
                            let _ = &(*EXTI::ptr()).ftsr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Configures the line to trigger interrupts on both edges
                    pub fn into_both_edge_triggered (
                        self,
                        ) -> $PXi<Enabled<Both>> {
                        unsafe {
                            let _ = &(*EXTI::ptr()).imr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                            let _ = &(*EXTI::ptr()).rtsr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                            let _ = &(*EXTI::ptr()).ftsr.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                        };
                        $PXi {
                            _mode: PhantomData
                        }
                    }

                    /// Check the pending flag
                    pub fn pending(&self) -> bool {
                        unsafe{
                            (&(*EXTI::ptr()).pr.read().bits() & (0b1 << $i)) != 0
                        }
                    }

                    /// Clear the pending flag
                    pub fn clear(&mut self) {
                        unsafe{
                            let _ = &(*EXTI::ptr()).pr.write(|w| w.bits(0b1 << $i));
                        }
                    }
                }

                impl<MODE> $PXi<Enabled<MODE>> {
                    /// Trigger software interrupt
                    pub fn set_pending(&mut self)  {
                        unsafe {
                            let _ = &(*EXTI::ptr()).swier.modify(|r, w| w.bits(r.bits() | (0b1 << $i)));
                        }
                    }
                }
            )+
    }
}

exti!(
    EXTI0: (exti0, 0, Disabled),
    EXTI1: (exti1, 1, Disabled),
    EXTI2: (exti2, 2, Disabled),
    EXTI3: (exti3, 3, Disabled),
    EXTI4: (exti4, 4, Disabled),
    EXTI5: (exti5, 5, Disabled),
    EXTI6: (exti6, 6, Disabled),
    EXTI7: (exti7, 7, Disabled),
    EXTI8: (exti8, 8, Disabled),
    EXTI9: (exti9, 9, Disabled),
    EXTI10: (exti10, 10, Disabled),
    EXTI11: (exti11, 11, Disabled),
    EXTI12: (exti12, 12, Disabled),
    EXTI13: (exti13, 13, Disabled),
    EXTI14: (exti14, 14, Disabled),
    EXTI15: (exti15, 15, Disabled),
    EXTI16: (exti16, 16, Disabled),
    EXTI17: (exti17, 17, Disabled),
    EXTI19: (exti19, 19, Disabled),
    EXTI20: (exti20, 20, Disabled),
    EXTI21: (exti21, 21, Disabled),
    EXTI22: (exti22, 22, Disabled),
    EXTI23: (exti23, 23, Disabled),
    EXTI24: (exti24, 24, Disabled),
    EXTI25: (exti25, 25, Disabled),
    EXTI26: (exti26, 26, Disabled),
    EXTI28: (exti28, 28, Disabled),
    EXTI29: (exti29, 29, Disabled),
);
