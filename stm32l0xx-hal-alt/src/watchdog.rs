//! Watchdog timers

use crate::pac::IWDG;

/// Independent Watchdog Timer prescale divisor
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Debug, defmt::Format)]
#[allow(non_camel_case_types)]
pub enum Prescale {
    /// Divide LSI by 4
    Div_4 = 0b000,
    /// Divide LSI by 8
    Div_8 = 0b001,
    /// Divide LSI by 16
    Div_16 = 0b010,
    /// Divide LSI by 32
    Div_32 = 0b011,
    /// Divide LSI by 64
    Div_64 = 0b100,
    /// Divide LSI by 128
    Div_128 = 0b101,
    /// Divide LSI by 256
    Div_256 = 0b110,
}

/// Independent watchdog timer
pub struct IndependentWatchdog {
    iwdg: IWDG,
}

impl IndependentWatchdog {
    /// Get the independent watchdog interface
    #[inline(always)]
    pub fn constrain(iwdg: IWDG, prescalar: Prescale, reload: u16) -> Self {
        iwdg.kr.write(|w| w.key().reset());
        iwdg.kr.write(|w| w.key().enable());

        while iwdg.sr.read().pvu().bit() {}
        iwdg.pr.write(|w| w.pr().bits(prescalar as u8));

        while iwdg.sr.read().rvu().bit() {}
        iwdg.rlr.write(|w| w.rl().bits(reload));

        iwdg.kr.write(|w| w.key().start());
        iwdg.kr.write(|w| w.key().reset());
        defmt::debug! {"iwdg started w/ pre: {} rld: {=u16}", prescalar, reload};

        Self { iwdg }
    }

    /// Return the independent watchdog interface
    #[inline(always)]
    pub fn free(self) -> IWDG {
        self.iwdg
    }

    /// Return the independent watchdog interface
    #[inline(always)]
    pub fn feed(&mut self) {
        self.iwdg.kr.write(|w| w.key().reset());
        defmt::trace!("iwdg reset");
    }
}
