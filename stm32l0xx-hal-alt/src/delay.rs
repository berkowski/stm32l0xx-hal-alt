//! Blocking delay interfaces
use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use embedded_time::{
    duration::{Microseconds, Milliseconds},
    fraction::Fraction,
};

use crate::{pac, rcc::Rcc};

use core::convert::TryFrom;

pub struct Delay<T> {
    /// Timer peripheral
    timer: T,
    /// Frequency of timer clock in Hz
    ticks_per_us: Fraction,
}

impl Delay<pac::SYST> {
    const MAX_RVR: u32 = 0x00FF_FFFF;

    pub fn constrain(mut timer: pac::SYST, rcc: &mut Rcc) -> Self {
        timer.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        let clock_freq = rcc.clocks.ahb_clk();
        let ticks_per_us = embedded_time::fraction::Fraction::new(clock_freq.0, 1_000_000);
        Self {
            timer,
            ticks_per_us,
        }
    }

    pub fn free(self) -> pac::SYST {
        self.timer
    }

    pub fn delay_us(&mut self, us: Microseconds<u32>) {
        let mut total = self
            .ticks_per_us
            .checked_mul(&Fraction::from_integer(us.0))
            .map_or(u32::MAX, |f| f.to_integer());

        while total > 0 {
            let current = if total <= Self::MAX_RVR {
                total
            } else {
                Self::MAX_RVR
            };
            self.timer.set_reload(current);
            self.timer.clear_current();
            self.timer.enable_counter();
            total -= current;
            while !self.timer.has_wrapped() {}
            self.timer.disable_counter();
        }
    }

    pub fn delay_ms(&mut self, ms: Milliseconds<u32>) {
        let us = Microseconds::<u32>::try_from(ms).unwrap_or(Microseconds(u32::MAX));
        self.delay_us(us)
    }
}

impl DelayUs<u8> for Delay<pac::SYST> {
    fn delay_us(&mut self, us: u8) {
        self.delay_us(Microseconds(us as u32))
    }
}

impl DelayUs<u16> for Delay<pac::SYST> {
    fn delay_us(&mut self, us: u16) {
        self.delay_us(Microseconds(us as u32))
    }
}

impl DelayUs<u32> for Delay<pac::SYST> {
    fn delay_us(&mut self, us: u32) {
        self.delay_us(Microseconds(us))
    }
}
impl DelayMs<u8> for Delay<pac::SYST> {
    fn delay_ms(&mut self, ms: u8) {
        self.delay_ms(Milliseconds(ms as u32))
    }
}
impl DelayMs<u16> for Delay<pac::SYST> {
    fn delay_ms(&mut self, ms: u16) {
        self.delay_ms(Milliseconds(ms as u32))
    }
}

impl DelayMs<u32> for Delay<pac::SYST> {
    fn delay_ms(&mut self, ms: u32) {
        self.delay_ms(Milliseconds(ms))
    }
}
